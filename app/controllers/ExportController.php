<?php

class ExportController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getExport($type,$id)
	{
		switch($type)
		{
				case 'employ':
					$date = date('Y-m-d');
					$sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.lock_mudjam','tb_ticket.created_at','tb_categories.cat_price','users.uname','users.lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->where('tb_ticket.pid',$id)->orderBy('tb_ticket.id','desc')
				->get();

					$count_num = Ticket::where('pid',$id)->count();
					$count_mudjam = Ticket::where('pid',$id)->sum('tb_ticket.lock_mudjam');
					$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->where('tb_ticket.pid',$id)->sum('tb_categories.cat_price');
					$c = Employee::where(array('pid'=>$id))->first();
					Excel::create('รายงานจองบู๊ท คุณ'.$c->e_name.' '.$c->e_lname,function($excel)  use ($sql,$count_num,$count_price,$c,$date,$id,$count_mudjam) {

    				$excel->setTitle('รายงานจองบู๊ทงานกาชาดจังหวัดสุรินทร์ ประจำปี 2561');

					$excel->sheet('sheet1',function($sheet)  use ($sql,$count_num,$count_price,$c,$date,$id,$count_mudjam)  {
						// $sheet->setOrientation('landscape');
						 $sheet->setPageMargin(array(0.75, 0.75, 0.75, 0.75));
						 $sheet->setHeight(1, 24);
					// 	 $sheet->mergeCells('A1:H1');
					// 	 $sheet->mergeCells('A3:H3');
		 	 // $sheet->cell('A1', function($cell) {
						 
					// 	 							$cell->setAlignment('center');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
					// 								 	 $sheet->cell('A3', function($cell) {
						 
					// 	 							 $cell->setAlignment('right');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
						 
						 $sheet->setStyle(array(
							'font' => array(
								'name'      =>  'TH SarabunPSK',
							 'size'      =>  16,
							//'bold'      =>  true
							)
						));
						 $sheet->loadView('export.exportemploy',array('data'=>$sql,'count_num'=>$count_num,'count_price'=>$count_price,'n'=>$c,'date'=>$date,'id'=>$id,'count_mudjam'=>$count_mudjam));
    	});


		})->download('xls');
	     
				break;

		case 'zone':
					$date = date('Y-m-d');
					$sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.lock_mudjam','tb_ticket.created_at','tb_categories.cat_price','users.uname','users.lname','tb_employer.e_name','tb_employer.e_lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->join('tb_employer','tb_ticket.pid','=','tb_employer.pid')
					->where('tb_ticket.zone',$id)->orderBy('tb_ticket.id','desc')
				->get();

					$count_num = Ticket::where('zone',$id)->count();
					$count_mudjam = Ticket::where('zone',$id)->sum('tb_ticket.lock_mudjam');
					$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->where('tb_ticket.zone',$id)->sum('tb_categories.cat_price');
				//	$c = Employee::where(array('pid'=>$id))->first();
					Excel::create('รายงานจองบู๊ท '.$id,function($excel)  use ($sql,$count_num,$count_price,$date,$id,$count_mudjam) {

    				$excel->setTitle('รายงานจองบู๊ทงานกาชาดจังหวัดสุรินทร์ ประจำปี 2561');

					$excel->sheet('sheet1',function($sheet)  use ($sql,$count_num,$count_price,$date,$id,$count_mudjam)  {
						// $sheet->setOrientation('landscape');
						 $sheet->setPageMargin(array(0.75, 0.75, 0.75, 0.75));
						 $sheet->setHeight(1, 24);
					// 	 $sheet->mergeCells('A1:I1');
					// 	 $sheet->mergeCells('A3:I3');
		 	 // $sheet->cell('A1', function($cell) {
						 
					// 	 							$cell->setAlignment('center');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
					// 								 	 $sheet->cell('A3', function($cell) {
						 
					// 	 							 $cell->setAlignment('right');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
						 
						 $sheet->setStyle(array(
							'font' => array(
								'name'      =>  'TH SarabunPSK',
							 'size'      =>  16,
							//'bold'      =>  true
							)
						));
						 $sheet->loadView('export.exportzone',array('data'=>$sql,'count_num'=>$count_num,'count_price'=>$count_price,'date'=>$date,'id'=>$id,'count_mudjam'=>$count_mudjam));
    	});


		})->download('xls');
	     
				break;
		}
 		}

 		public function getExportDate($type,$date1,$date2,$id)
	{
		switch($type)
		{
				case 'employ':
					$date = date('Y-m-d');
					$sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.lock_mudjam','tb_ticket.created_at','tb_categories.cat_price','users.uname','users.lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->where('tb_ticket.pid',$id)
					->whereBetween('tb_ticket.created_at',array($date1,$date2))
					->orderBy('tb_ticket.id','desc')
				->get();

					$count_num = Ticket::where('pid',$id)->count();
					$count_mudjam = Ticket::where('pid',$id)->whereBetween('tb_ticket.created_at',array($date1,$date2))->sum('tb_ticket.lock_mudjam');
					$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->where('tb_ticket.pid',$id)->whereBetween('tb_ticket.created_at',array($date1,$date2))->sum('tb_categories.cat_price');
					$c = Employee::where(array('pid'=>$id))->first();
					Excel::create('รายงานจองบู๊ท คุณ'.$c->e_name.' '.$c->e_lname.' วันที่ '.Helpers::changeDate($date1).'-'.Helpers::changeDate($date2),function($excel)  use ($sql,$count_num,$count_price,$c,$date,$id,$date1,$date2,$count_mudjam) {

    				$excel->setTitle('รายงานจองบู๊ทงานกาชาดจังหวัดสุรินทร์ ประจำปี 2561');

					$excel->sheet('sheet1',function($sheet)  use ($sql,$count_num,$count_price,$c,$date,$id,$date1,$date2,$count_mudjam)  {
						// $sheet->setOrientation('landscape');
						 $sheet->setPageMargin(array(0.75, 0.75, 0.75, 0.75));
						 $sheet->setHeight(1, 24);
					// 	 $sheet->mergeCells('A1:H1');
					// 	 $sheet->mergeCells('A3:H3');
		 	 // $sheet->cell('A1', function($cell) {
						 
					// 	 							$cell->setAlignment('center');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
					// 								 	 $sheet->cell('A3', function($cell) {
						 
					// 	 							 $cell->setAlignment('right');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
						 
						 $sheet->setStyle(array(
							'font' => array(
								'name'      =>  'TH SarabunPSK',
							 'size'      =>  16,
							//'bold'      =>  true
							)
						));
						 $sheet->loadView('export.exportemploydate',array('data'=>$sql,'count_num'=>$count_num,'count_price'=>$count_price,'n'=>$c,'date'=>$date,'id'=>$id,'date1'=>$date1,'date2'=>$date2,'count_mudjam'=>$count_mudjam));
    	});


		})->download('xls');
	     
				break;

		case 'zone':
					$date = date('Y-m-d');
					$sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.lock_mudjam','tb_ticket.created_at','tb_categories.cat_price','users.uname','users.lname','tb_employer.e_name','tb_employer.e_lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->join('tb_employer','tb_ticket.pid','=','tb_employer.pid')
					->where('tb_ticket.zone',$id)
					->whereBetween('tb_ticket.created_at',array($date1,$date2))	
					->orderBy('tb_ticket.id','desc')
				->get();
					$count_mudjam = Ticket::where('zone',$id)->whereBetween('tb_ticket.created_at',array($date1,$date2))->sum('tb_ticket.lock_mudjam');
					$count_num = Ticket::where('zone',$id)->count();
					$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->where('tb_ticket.zone',$id)->whereBetween('tb_ticket.created_at',array($date1,$date2))->sum('tb_categories.cat_price');
				//	$c = Employee::where(array('pid'=>$id))->first();
					Excel::create('รายงานจองบู๊ท '.$id.' วันที่ '.Helpers::changeDate($date1).'-'.Helpers::changeDate($date2),function($excel)  use ($sql,$count_num,$count_price,$date,$id,$date1,$date2,$count_mudjam) {

    				$excel->setTitle('รายงานจองบู๊ทงานกาชาดจังหวัดสุรินทร์ ประจำปี 2561');

					$excel->sheet('sheet1',function($sheet)  use ($sql,$count_num,$count_price,$date,$id,$date1,$date2,$count_mudjam)  {
						// $sheet->setOrientation('landscape');
						 $sheet->setPageMargin(array(0.75, 0.75, 0.75, 0.75));
						 $sheet->setHeight(1, 24);
						 // $sheet->mergeCells('A1:I1');
						 // $sheet->mergeCells('A3:I3');
		 	 // $sheet->cell('A1', function($cell) {
						 
					// 	 							$cell->setAlignment('center');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
					// 								 	 $sheet->cell('A3', function($cell) {
						 
					// 	 							 $cell->setAlignment('right');
					// 	 							$cell->setFontFamily('TH SarabunPSK');
						 
					// 	 							});
						 
						 $sheet->setStyle(array(
							'font' => array(
								'name'      =>  'TH SarabunPSK',
							 'size'      =>  16,
							//'bold'      =>  true
							)
						));
						 $sheet->loadView('export.exportdatezone',array('data'=>$sql,'count_num'=>$count_num,'count_price'=>$count_price,'date'=>$date,'id'=>$id,'date1'=>$date1,'date2'=>$date2,'count_mudjam'=>$count_mudjam));
    	});


		})->download('xls');
	     
				break;
		}
 		}
}