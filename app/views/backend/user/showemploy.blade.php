@extends('masterbackend')
@section('content')
       <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}} คุณ {{$c->e_name}} {{ $c->e_lname}} จำนวน <span class="label label-success">{{ Helpers::UserStatusLock($id)}}/{{ $count_num}}</span> ล๊อค รวม <span class="label label-primary">{{ number_format(Helpers::UserStatusPrice($id))}}/{{ number_format($count_price)}}</span> บาท  มัดจำ  <span class="label label-warning">{{ number_format($count_mudjam)}}</span> บาท   ค้างชำระ <span class="label label-danger">{{ number_format($count_price-$count_mudjam)}}</span> บาท
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
       
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ Lang::get('msg.list-item',array(), 'th') }}</h3>
               
               
              

             
            </div>
            
            
            <div class="box-body">
              <div class="row">
        <div class="col-md-4 pull-right">
      <div class="form-group">
              <label></label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservation">
           <div class="input-group-btn">
                  <button type="button" class="btn btn-danger" id="search"><i class="fa fa-search"></i> ค้นหา</button>
                </div>
                </div>
                <!-- /.input group -->
              </div>
        </div>
        </div>
 
            <div id="showdata">
               <div class="box-tools pull-right">
               <a href="{{ URL::to('backend/export/employ',$id) }}"   class="btn btn-info"><i class="fa fa-print"></i> รายงานการจอง</a>
               
           </div>  <br><br>
              <table id="Mainmenu_data" class="table table-bordered table-striped" width="100%">
                <thead>
             
                   
                    <th width="5%">{{ Lang::get('msg.msg_no', array(), 'th') }}</th> 
                  <th  >โซน </th>
                  <th width="" >ล๊อค </th>
                   
                  <th>มัดจำ/ราคา</th>
                  <th>เหลือ</th>
        
                  <th>วันที่จอง</th>
                  <th>รูปแบบการจอง</th>
                  <th>การชำระเงิน</th>
                 
                   <th>ผู้บันทึก</th>
                  <th >{{ Lang::get('msg.msg_tools', array(), 'th') }}</th>
                </thead>
				<tbody><?php $i=1;?>
				@foreach($sql as $data=>$d)
				
				<tr>
				<td  align="center">
				{{ $i}}
				</td>
				<td align="center">
				<a href="{{ URL::to('backend/showzone',$d->zone)}}">{{$d->zone}}</a>
				</td>
				<td>{{$d->lock}}</td>
				<td>{{ $d->lock_mudjam}}/{{ $d->cat_price}}</td>
				 <td>{{number_format($d->cat_price-$d->lock_mudjam)}}</td>
				<td>{{ Helpers::changeDate($d->created_at)}} </td>
        <td>{{ Helpers::LockType($d->locktype)}}</td>
        <td><a @if($d->lockstatus==0) class="btn btn-warning" href="{{URL::to('backend/checkpay',array($d->tid,$id))}}" onclick="javascript:return confirm('ยืนยันการชำระเงิน โซน {{$d->zone}} ล๊อก {{ $d->lock}}\n ของคุณ{{$c->e_name}} {{$c->e_lname}} จริงหรือไม่?')" @else class="btn btn-success" @endif >{{ Helpers::LockStatus($d->lockstatus)}}</a></td>
				<td>{{ $d->uname}} {{ $d->lname}}</td>
	 
				<td align="center">
          @if($d->lockstatus==0)
				  <a href="{{ URL::to('backend/delzoneuser',array($d->zone,$d->lock,$id))}}" class="btn btn-danger" onclick="javascript:return confirm('ต้องการยกเลิกการจองบู๊ท โซน {{$d->zone}} ล๊อก {{ $d->lock}}\n ของคุณ{{$c->e_name}} {{$c->e_lname}} จริงหรือไม่?')">
              <i class="fa fa-close"></i> {{ Lang::get('msg.msg_cancle',array(), 'th')}}การจอง</a>
          @endif
				</td>
				</tr>
				<?php $i++; ?>
				@endforeach
				</tbody>
              </table>
            </div><!-- /.box-body -->
             
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
{{ Session::get('status') }}
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_tag',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">
   <input type="hidden"  id="pid" value="{{$id}}">

@stop
@section('script')
@section('script')
<script src="{{ URL::to('js/min_moment.min.js')}}"></script>
<script src="{{ URL::to('js/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{ URL::to('js/bootstrap-datepicker.min.js')}}"></script>
<script>
  $(function () {
  
    $('#Mainmenu_data').DataTable({
	 "pageLength": 100
	});
   //Date range picker
  // moment.locale('th');
  moment.updateLocale('th', {
    longDateFormat : {
               L: "DD/MM/YYYY",
      
    }
});
  // moment().format("dddd, MMMM Do YYYY"); 
    $('#reservation').daterangepicker()
    //Date range picker with time picker
   // $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY h:mm A' })
 
  })
 
  $('#search').click(function(){

   var zonedate= $('#reservation').val();
  var spd = zonedate.split('-');
  var d1 = spd[0].split('/');
  var dd1 = d1[2]+'-'+d1[1]+'-'+d1[0];
  var d2 = spd[1].split('/');
  var dd2 = d2[2]+'-'+d2[1]+'-'+d2[0];
  var date1 = dd1.replace(' ','');
  var date2 = dd2.replace(' ','');

    var pid= $('#pid').val();
 
   var Url = "{{URL::to('')}}";
//    $('#showdata').load(Url+'/backend/showdatazone/'+zd2+'/'+zd4+'/'+zone,{
$('#showdata').load(Url+'/backend/showemploydata', { "date1": date1,"date2":date2,"pid":pid });
  });

</script>
@stop
 