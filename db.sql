﻿-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.20-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------
 


 

-- Dumping structure for table db_ticket.tb_categories
CREATE TABLE IF NOT EXISTS `tb_categories` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `categories_name` varchar(25) NOT NULL,
  `cat_id` varchar(25) NOT NULL,
  `cat_num` varchar(15) DEFAULT NULL,
  `cat_price` varchar(15) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `create_by` int(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table db_ticket.tb_categories: ~11 rows (approximately)
/*!40000 ALTER TABLE `tb_categories` DISABLE KEYS */;
INSERT IGNORE INTO `tb_categories` (`id`, `categories_name`, `cat_id`, `cat_num`, `cat_price`, `created_at`, `updated_at`, `create_by`) VALUES
	(1, 'โซน A1', 'A1', '97', '16000', '2018-08-25 08:36:28', '2018-08-26 16:14:13', 4),
	(2, 'โซน A2', 'A2', '90', '16000', '2018-08-26 21:42:57', '2018-08-26 21:43:00', 4),
	(3, 'โซน A3', 'A3', '29', '16000', '2018-08-26 21:46:34', '2018-08-26 21:46:35', 4),
	(4, 'โซน A4', 'A4', '28', '16000', '2018-08-26 21:48:18', '2018-08-26 21:48:22', 4),
	(5, 'โซน A5', 'A5', '51', '16000', '2018-08-26 21:48:19', '2018-08-26 21:48:24', 4),
	(6, 'โซน A2', 'A6', '29', '16000', '2018-08-26 21:48:21', '2018-08-26 21:48:25', 4),
	(7, 'โซน A ล่าง', 'AA', '67', '10000', '2018-08-26 21:51:38', '2018-08-26 21:51:39', 4),
	(8, 'โซน B1', 'B1', '47', '10000', '2018-08-26 21:53:52', '2018-08-26 21:53:53', 4),
	(9, 'โซน B2', 'B2', '42', '10000', '2018-08-26 21:54:33', '2018-08-26 21:54:35', 4),
	(10, 'โซน B3', 'B3', '47', '16000', '2018-08-26 21:56:58', '2018-08-26 21:57:00', 4),
	(11, 'โซน B4', 'B4', '60', '16000', '2018-08-26 21:57:50', '2018-08-26 21:57:51', 4);
/*!40000 ALTER TABLE `tb_categories` ENABLE KEYS */;

-- Dumping structure for table db_ticket.tb_employer
CREATE TABLE IF NOT EXISTS `tb_employer` (
  `id` int(13) NOT NULL AUTO_INCREMENT,
  `pid` varchar(50) NOT NULL,
  `e_name` varchar(50) NOT NULL,
  `e_lname` varchar(50) NOT NULL,
  `e_company` varchar(100) NOT NULL,
  `e_tel` varchar(50) NOT NULL,
  `e_address` varchar(150) NOT NULL,
  `create_by` int(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_ticket.tb_employer: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_employer` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_employer` ENABLE KEYS */;

-- Dumping structure for table db_ticket.tb_ticket
CREATE TABLE IF NOT EXISTS `tb_ticket` (
  `id` int(13) NOT NULL AUTO_INCREMENT,
  `pid` varchar(50) DEFAULT NULL,
  `zone` varchar(50) DEFAULT NULL,
  `lock` varchar(50) DEFAULT NULL,
  `locktype` int(1) DEFAULT NULL,
  `lockstatus` int(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_ticket.tb_ticket: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_ticket` ENABLE KEYS */;

-- Dumping structure for table db_ticket.tb_unticket
CREATE TABLE IF NOT EXISTS `tb_unticket` (
  `id` int(13) NOT NULL AUTO_INCREMENT,
  `pid` varchar(50) DEFAULT NULL,
  `zone` varchar(50) DEFAULT NULL,
  `lock` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_ticket.tb_unticket: ~0 rows (approximately)
/*!40000 ALTER TABLE `tb_unticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_unticket` ENABLE KEYS */;

-- Dumping structure for table db_ticket.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photoURL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` int(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table db_ticket.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `uname`, `lname`, `phone`, `email`, `username`, `password`, `photoURL`, `user_status`, `created_at`, `updated_at`, `remember_token`) VALUES
	(4, 'ผู้ดูแล', 'ทดสอบระบบ', '0885959562', 'นักศึกษาฝีกงาน', 'admin', '$2y$10$TDvbzYBWFo2y1u6KfkJylOKWkxVOzlZGlYeVuJMIHmvMAMf1FBBsi', 'ig4s1yNqVsOkTkdR.png', 1, '2018-08-24 06:07:42', '2018-08-27 09:53:50', 'YTxSQSbNxlTXETmiYvrGOXt4djpRSkh8mQf88ki43MsBJbJtho5frg8XtBCl');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
