<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getShowZoneView($zone,$lock)
{
	$sql = Employee::select('tb_employer.pid','tb_employer.e_name','tb_employer.e_lname','tb_employer.e_tel','tb_employer.e_address','tb_employer.e_company','tb_ticket.zone','tb_ticket.lock')->join('tb_ticket','tb_ticket.pid','=','tb_employer.pid')->first();
	$cat = Categories::where('cat_id',$zone)->first();
	return View::make('backend.view')->with(
	 array(
	 	'title' => 'ข้อมูลการจองบู๊ท',
	 	'rules'		=>'',
	 	'zone'		=>$zone,
	 	'lock'		=>$lock,
	 	'sql'		=>$sql,
	 	'price'   	=>$cat->cat_price,
	 	'count'		=>$cat->cat_num,	

	 ));
}
public function getShowStat()
{
		$sql = Categories::all();
		$count_lock = Categories::sum('cat_num');
		$sumall = Ticket::where('tb_ticket.lockstatus','0')->join('tb_categories','tb_categories.cat_id','=','tb_ticket.zone')->sum('tb_categories.cat_price');
		$sumall_total = Ticket::where('tb_ticket.lockstatus','1')->join('tb_categories','tb_categories.cat_id','=','tb_ticket.zone')->sum('tb_categories.cat_price');
		$count_lock_pay = Ticket::where('tb_ticket.lockstatus','1')->count();
		$count_mudj = Ticket::sum('tb_ticket.lock_mudjam');
		return View::make('backend.stat')->with(
			array(
			'title' 	=>"ข้อมูลโซนร้านค้า",
			'sql'	=> $sql,
			'status'	=> 'null',
			'count_lock' => number_format($count_lock),
			'sumall'	=> number_format($sumall),
			'sumall_total'	=> number_format($sumall_total),
			'count_lock_pay'=>number_format($count_lock_pay),
			'count_mudj'=> number_format($count_mudj)
			));
}


}
