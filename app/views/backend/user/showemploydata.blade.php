    <link rel="stylesheet" href="{{ URL::to('css/bootstrap.css') }}">
   <link rel="stylesheet" href="{{ URL::to('css/AdminLTE.css') }} ">
   <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }} ">
               
               
              

              <div class="box-tools pull-right">
                <a href="{{URL::to('backend/exportdate/employ',array($date1,$date2,$id))}}"   class="btn btn-info"><i class="fa fa-print"></i> รายงานการจอง</a>
               
           </div>  
        
            <br><h3>รายงานช่วงวันที่ <small>{{ Helpers::changeDate($date1)}} ถึง {{ Helpers::changeDate($date2)}}</small></h3><br>
 

              <table id="Mainmenu_data" class="table table-bordered table-striped" width="100%">
                <thead>
             
                   
                    <th width="5%">{{ Lang::get('msg.msg_no', array(), 'th') }}</th> 
                  <th  >โซน </th>
                  <th width="" >ล๊อค </th>
                   
                  <th>ราคา</th>
                  <th>เหลือ</th>
        
                  <th>วันที่จอง</th>
                  <th>รูปแบบการจอง</th>
                  <th>การชำระเงิน</th>
                 
                   <th>ผู้บันทึก</th>
                  <th >{{ Lang::get('msg.msg_tools', array(), 'th') }}</th>
                </thead>
				<tbody><?php $i=1;?>
				@foreach($sql as $data=>$d)
				
				<tr>
				<td  align="center">
				{{ $i}}
				</td>
				<td align="center">
				<a href="{{ URL::to('backend/showzone',$d->zone)}}">{{$d->zone}}</a>
				</td>
				<td>{{$d->lock}}</td>
				<td>{{ $d->lock_mudjam}}/{{ $d->cat_price}}</td>
				 <td>{{number_format($d->cat_price-$d->lock_mudjam)}}</td>
				<td>{{ Helpers::changeDate($d->created_at)}} </td>
        <td>{{ Helpers::LockType($d->locktype)}}</td>
        <td><a @if($d->lockstatus==0) class="btn btn-warning" href="{{URL::to('backend/checkpay',array($d->tid,$id))}}" onclick="javascript:return confirm('ยืนยันการชำระเงิน โซน {{$d->zone}} ล๊อก {{ $d->lock}}\n ของคุณ{{$d->e_name}} {{$d->e_lname}} จริงหรือไม่?')" @else class="btn btn-success" @endif >{{ Helpers::LockStatus($d->lockstatus)}}</a></td>
				<td>{{ $d->uname}} {{ $d->lname}}</td>
	 
				<td align="center">
          @if($d->lockstatus==0)
				  <a href="{{ URL::to('backend/delzoneuser',array($d->zone,$d->lock,$id))}}" class="btn btn-danger" onclick="javascript:return confirm('ต้องการยกเลิกการจองบู๊ท โซน {{$d->zone}} ล๊อก {{ $d->lock}}\n ของคุณ{{$d->e_name}} {{$d->e_lname}} จริงหรือไม่?')">
              <i class="fa fa-close"></i> {{ Lang::get('msg.msg_cancle',array(), 'th')}}การจอง</a>
          @endif
				</td>
				</tr>
				<?php $i++; ?>
				@endforeach
				</tbody>
              </table>
            
    <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }} "></script>
    <script src="{{ asset('js/bootstrap.min.js') }} "></script>
   <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
 
<script>
  $(function () {$('#Mainmenu_data').DataTable({
	 "pageLength": 100
	}); });

</script>
 