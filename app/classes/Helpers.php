<?php
class Helpers {
	static function changeDate($date){
//ใช้ Function explode ในการแยกไฟล์ ออกเป็น  Array
$get_date = explode("-",$date);
 $date = explode(" ",$get_date["2"]);
//กำหนดชื่อเดือนใส่ตัวแปร $month
	$month = array("01"=>"ม.ค.","02"=>"ก.พ.","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
//month
$get_month = $get_date["1"];

//year	
$year = $get_date["0"]+543;

return $date[0]." ".$month[$get_month]." ".$year." ";

}
 	static function getCheckZone($id)
 	{
 		 $count = Ticket::where('zone',$id)->count();
 		 return $count;
 	}
 	static function getTotalZone($id)
 	{
 		$t = Categories::select('cat_num')->where('cat_id',$id)->first();
 		$count = Ticket::where('zone',$id)->count();
 		return $t->cat_num-$count;
 		
 	}
 	static function getTotalZonePrice($id)
 	{
 		$t = Categories::select('cat_price')->where('cat_id',$id)->first();
 		$count = Ticket::where('zone',$id)->count();
 		$cal = $t->cat_price*$count;
 	 
 		return number_format($cal);
 	}
 	static function getAllTotalZonePrice($id)
 	{
 		$t = Categories::select('cat_price','cat_num')->where('cat_id',$id)->first();
 		 
 		$total = $t->cat_num*$t->cat_price;
 		return number_format($total);
 	}
 	static function getAllTicket()
 	{
 		 $count = Ticket::count('lock');
 		 return $count;

 	}
 	static function ShowUser()
 	{
 		$u=User::where('id',Auth::user()->id)->first();
 		$name = $u->uname." ".$u->lname;
 		return $name;

 	}
 	static function LockStatus($id)
 	{


 		switch($id):
 			case '0':
 				return "ยังไม่ชำระ";
 			break;
 			case '1':
 				return "ชำระแล้ว";
 			break;

 		endswitch;
 	}
 	static function LockType($id)
 	{
 		 

 		switch($id):
 			case '1':
 				return "ตนเอง";
 			break;
 			case '2':
 				return "ทางโทรศัพท์";
 			break;

 		endswitch;
 	}
 	static function UserStatusLock($id)
 	{
 		$c = Ticket::where(array('pid'=>$id,'lockstatus'=>'1'))->count();
 		return $c;
 	}
 	static function UserStatusLockZone($id)
 	{
 		$c = Ticket::where(array('zone'=>$id,'lockstatus'=>'1'))->count();
 		return $c;
 	}
 	static function UserStatusPrice($id)
 	{
 		$c = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
				->where(array('tb_ticket.pid'=>$id,'tb_ticket.lockstatus'=>'1'))->sum('tb_categories.cat_price');

		return $c;
 	}
 	static function UserStatusPriceZone($id)
 	{
 		$c = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
				->where(array('tb_ticket.zone'=>$id,'tb_ticket.lockstatus'=>'1'))->sum('tb_categories.cat_price');

		return $c;
 	}

}