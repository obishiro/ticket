<?php

class ZoneController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	//	$title = Lang::get('msg.msg_user',array(),'th');
		//$api = URL::to('backend/data/user');
		$sql = Categories::all();
		$count_lock = Categories::sum('cat_num');
		

		return View::make('backend.zone.index')->with(
			array(
			'title' 	=>"ข้อมูลโซนร้านค้า",
			'sql'	=> $sql,
			'status'	=> 'null',
			'count_lock' => number_format($count_lock)
			));

		//return '=>'.Auth::user()->user_status;
	}
public function getAdd()
	{
		 		$title = Lang::get('msg.msg_user',array(),'th');
			//	$sql = Categories::orderBy('id','desc')->get();
			//	$prefix = Prefix::orderBy('prefix_name','asc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.zone.frm_addzone')->with(
				 array(
				 	'title' 	=>"ข้อมูลโซนร้านค้า",
				 	'rules'		=>$rules,
				//  	'sql'		=> $sql,
				 // 	'prefix'		=> $prefix,
				 	'status'	=> 'null'
				       ));
	}

	public function postAdd()
	{
		$c = new Categories;
		$c->categories_name = Input::get('txt_zonename');
		$c->cat_num = Input::get('txt_lock');
		$c->cat_price = Input::get('txt_price');
		$c->created_at = date('Y-m-d H:i:s');
		$c->updated_at = date('Y-m-d H:i:s');
		$c->create_by = Auth::user()->id;
		$c->save();
		 return Redirect::to('backend/zone')->with(
				array(
					'save-success' => 'save'
				       ));
	}

	public function getEditZone($id)
	{
		 		//$title = Lang::get('msg.msg_user',array(),'th');
				$c = Categories::find($id);
			//	$prefix = Prefix::orderBy('prefix_name','asc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.zone.editzone')->with(
				 array(
				 	'title' 	=>"แก้ข้อมูลโซนร้านค้า",
				 	'rules'		=>$rules,
				  	'c'		=> $c,
				 	'id'		=> $id,
				 	'status'	=> 'null'
				       ));
	}

	public function postEditZone()
	{
		$id = Input::get('id');
		Categories::where('id',$id)->update(array('cat_price'=>Input::get('txt_price')));
		return Redirect::to('backend/zone')->with(
				array(
					'save-success' => 'save'
				       ));

	}

	public function getShowZone($id)
	{
		$sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.created_at','tb_ticket.lock_mudjam','tb_categories.cat_price','users.uname','users.lname','tb_employer.pid','tb_employer.e_name','tb_employer.e_lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->join('tb_employer','tb_ticket.pid','=','tb_employer.pid')
					->where('tb_ticket.zone',$id)
					->orderBy('tb_ticket.id','desc')
				->get();

			$count_num = Ticket::where('zone',$id)->count();
			$count_mudjam = Ticket::where('zone',$id)->sum('tb_ticket.lock_mudjam');
			$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
				->where('tb_ticket.zone',$id)->sum('tb_categories.cat_price');
		//	$c = Employee::where(array('pid'=>$id))->first();
			return View::make('backend.zone.showzone')->with(
				array(
				'title'	=>'การจองบู๊ทโซน',
				'sql'	=>$sql,
				'status'=>'null',
		//		'c'	=>$c,
				'id'=>$id,
				'count_num'=>$count_num,
				'count_price'=>$count_price,
				'count_mudjam' =>$count_mudjam
			)	
			);
	}
public function postShowdataZone()
	{
	$id = Input::get('zone');
	$date1 = Input::get('date1');
	$date2 = Input::get('date2');

	 $sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.created_at','tb_ticket.lock_mudjam','tb_categories.cat_price','users.uname','users.lname','tb_employer.pid','tb_employer.e_name','tb_employer.e_lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->join('tb_employer','tb_ticket.pid','=','tb_employer.pid')
					->where('tb_ticket.zone',$id)
					->whereBetween('tb_ticket.created_at',array($date1,$date2))	
					->orderBy('tb_ticket.id','desc')
				->get();

			$count_num = Ticket::where('zone',$id)->count();
			$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
				->where('tb_ticket.zone',$id)->sum('tb_categories.cat_price');
		//	$c = Employee::where(array('pid'=>$id))->first();
			return View::make('backend.zone.showzonedata')->with(
				array(
				'title'	=>'การจองบู๊ทโซน',
				'sql'	=>$sql,
				'status'=>'null',
		//		'c'	=>$c,
				'id'=>$id,
				'count_num'=>$count_num,
				'count_price'=>$count_price,
				'date1' =>$date1,'date2'=>$date2
			)	
			);
	 
	}


}
