@extends('masterbackend')
@section('content')
       <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
       
        <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title" style="text-transform: uppercase;">บู๊ทที่จองโซน {{ $type}} ล๊อกที่ {{ $lock}}</h3>

              
            </div>
            <br>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                                <div class="box-primary ">
                 <br>
              
                  {{ Form::open(array(
                    'id'=>'form-content'
                    ,'role'=>'form'
                   ,'enctype'=>'multipart/form-data'
                    ,'url'=>'backend/check/addboot'
                    ), $rules)}}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="row">
                         <label for="">{{ Lang::get('msg.msg_pid', array(), 'th') }}</label>
                    <div class="form-group input-group  has-feedback @if ($errors->has('txt_pid')) has-error @endif">

                       {{Form::input('text', 'txt_pid', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required',
                               'maxlength'=>13,
                               'id'=>'txt_pid'

                              ))}}
                <div class="input-group-btn">
                  <button type="button" id="checkpid" class="btn btn-danger">ตรวจสอบ</button>
                </div>
                <!-- /btn-group -->
                
              </div>
                      
                   </div>
                      <div class="row">
                         
                     <div class="form-group col-md-6 has-feedback @if ($errors->has('txt_firstname')) has-error @endif">
					           
                      <label for="">{{ Lang::get('msg.msg_firstname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_firstname', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    <div class="form-group col-md-6  has-feedback @if ($errors->has('txt_lastname')) has-error @endif">
					 
                      <label for="">{{ Lang::get('msg.msg_lastname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_lastname', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                    <div class="row">
                     <div class="form-group col-md-6  has-feedback @if ($errors->has('txt_company')) has-error @endif">
					  
                      <label for="">{{ Lang::get('msg.web_company', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_company', '', 
                         array(
                               'class'=>'form-control',
                             
                               

                              ))}}
                    </div>
                     <div class="form-group col-md-6  has-feedback @if ($errors->has('txt_tel')) has-error @endif">
					    
                      <label for="">{{ Lang::get('msg.web_tel', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_tel', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                    
                    <div class="row">
                         
                         <div class="form-group col-md-12 has-feedback @if ($errors->has('txt_address')) has-error @endif">
						   
                      <label for="">{{ Lang::get('msg.web_address', array(), 'th') }}</label>
                        <textarea class="form-control" name="txt_address" id="" style="width: 100%" rows="3"></textarea>
                        </div>
                       
                         
                     
                        
                        </div>
                         <div class="form-group col-md-12  has-feedback @if ($errors->has('txt_tel')) has-error @endif">
              
                      <label for="">มัดจำการจอง</label>
                      {{Form::input('text', 'txt_mudjam', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                       
                     <div class="row">
                         
                         <div class="form-group col-md-6">
                      <label for="">รูปแบบการจอง</label>
                          <div class="radio">
                            <label for=""><input type="radio" value="1" name="txt_locktype" checked="true"> จองด้วยตัวเอง</label>
                             <label for=""><input type="radio" value="2" name="txt_locktype" > จองทางโทรศัพท์</label>
                          </div>
                        </div>
                        <div class="form-group col-md-6">
                      <label for="">สถานะการชำระเงิน</label>
                       <div class="radio">
                            <label for=""><input type="radio" value="1" name="txt_lockstatus">ชำระแล้ว</label>
                             <label for=""><input type="radio" value="0" name="txt_lockstatus"  checked="true" >ยังไม่ชำระ</label>
                          </div>
                        </div>
                        </div>
                       
                      </div>
                      
                    </div>
                    
                    
                <div class="form-group col-md-8">
                     <button type="button" id="bt-reset" class="btn btn-danger  pull-right">
              <i class="fa fa-close"></i> {{ Lang::get('msg.msg_cancle',array(), 'th')}}</button>
              <button type="submit" class="btn btn-primary ">
              <i class="fa fa-check-circle"></i> {{ Lang::get('msg.msg_submit',array(), 'th')}}
               </button>
         </div>

                 </div><!-- /.box-body -->

                   
          
          
              <input type="hidden" name="key" value="{{ Str::random(16,'numberic') }}" >
              <input type="hidden" name="txt_zone" value="{{ $type}}">
              <input type="hidden" name="txt_lock" value="{{ $lock}}">
          
            </div><!-- /.box-body -->
             
                
              {{ Form::close()}}

                  
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
              <!-- Info Boxes Style 2 -->
              <h3>รายละเอียดบู๊ท {{$type}}</h3>
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">ราคา/บู๊ท</span>
              <span class="info-box-number">{{number_format($price)}} บาท</span>

              <div class="progress">
                
              </div>
              <span class="progress-description">
                   {{--  50% Increase in 30 Days --}}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-bank"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">จำนวนบู๊ท ทั้งหมด</span>
              <span class="info-box-number">{{$count}} ล๊อค</span>

              <div class="progress">
               
              </div>
              <span class="progress-description">
              {{--       20% Increase in 30 Days --}}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-cart-arrow-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">จำนวนบู๊ทที่จองแล้ว</span>
              <span class="info-box-number">{{ Helpers::getCheckZone($type)}} ล๊อค</span>

              <div class="progress">
            
              </div>
              <span class="progress-description">
                   
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa  fa-credit-card"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">คงเหลือ</span>
              <span class="info-box-number">{{ Helpers::getTotalZone($type)}} ล๊อค</span>

              <div class="progress">
               
              </div>
              <span class="progress-description">
                   
                  </span>
            </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


        </section><!-- /.content -->
    </div>
 
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_content',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')

         <script type="text/javascript">
             $(document).ready(function(){
          // $("#form-content").validetta({ 
          //   display : 'inline',
          //  errorTemplateClass : 'validetta-inline'});
           $("#checkpid").click(function(){

                var pid = $('#txt_pid').val();
                if(pid=="")
                {
                  alert("กรุณากรอกเลขบัตรประจำตัวประชาชนด้วย!");
                }
                $.ajax({

                  url: "{{URL::to('backend/checkpid')}}"+'/'+pid,
                  type: "get",
                   
                  success: function (response) {
                     if(response==0)
                     {
                      alert("บุคคลนี้ยังไม่เคยลงทะเบียนจองบู๊ท กรุณาบันทึกข้อมูลใหม่");
                      
                     }else{
                      alert("บุคคลนี้เคยลงทะเบียนจองบู๊ทแล้ว");
                      $('input[name="txt_firstname"]').val( "-");
                      $('input[name="txt_lastname"]').val( "-");
                      $('textarea[name="txt_address"]').val( "-");
                      $('input[name="txt_company"]').val( "-");
                      $('input[name="txt_tel"]').val( "-");
                      $('input[name="txt_mudjam"]').val( "0");
                     }
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                     console.log(textStatus, errorThrown);
                  }
                });

              });
         });              
       
         var msg = $('#lang').val();
         
              $('#status_save').show(0).delay(2000).slideUp();
               
               
              $('#bt-reset').click(function(){
               
                window.location.href='{{ URL::to("backend/boot")}}';
              });



        </script>
 
      
         
@stop
