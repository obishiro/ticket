@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
			 
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">แก้ไขข้อมูลคุณ {{ $c->e_name}} {{$c->e_lname}}</h3>
               
               
              

             
            </div>
            
            <div class="row" >
              <div class="col-md-5 col-sm-6 col-xs-12" 
              @if(Session::has('status'))
              id ="null"
              @endif
              @if(Session::has('save-success'))
               id="status_save" 
              @endif
              @if(Session::has('edit-success'))
               id="status_save" 
              @endif
              @if(Session::has('del-success'))
               id="status_save" 
              @endif
                style="margin-top:10px;margin-left:30%;  display:none" >
                 @if(Session::has('save-success'))
                  <div class="info-box bg-green">
                 @endif
                 @if(Session::has('edit-success'))
                  <div class="info-box bg-teal">
                 @endif
                 @if(Session::has('del-success'))
                  <div class="info-box bg-red-active">
                 @endif
                <span class="info-box-icon">
                  @if(Session::has('save-success'))
                  <i class="fa fa-save"></i>
                  @endif
                  @if(Session::has('del-success'))
                  <i class="fa fa-trash"></i>
                  @endif
                  @if(Session::has('edit-success'))
                  <i class="fa fa-pencil">
                  @endif
                  </i>
                </span>
                <div class="info-box-content">
                  <span class="info-box-text">{{ Lang::get('msg.msg_result', array(), 'th') }}</span>
                  <span class="info-box-number">
                    @if(Session::has('save-success'))
                    {{ Lang::get('msg.msg_save_success', array(), 'th') }}
                    @endif
                    @if(Session::has('del-success'))
                    {{ Lang::get('msg.msg_del_success', array(), 'th') }}
                    @endif
                    @if(Session::has('edit-success'))
                    {{ Lang::get('msg.msg_edit_success', array(), 'th') }}
                    @endif
                  </span>
                   </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              @if(Session::has('save-success') || Session::has('del-success') || Session::has('edit-success'))
               </div>
              @endif

              
            </div>
            <div class="box-body">
               <div class=" box-primary">
                 
              
                  {{ Form::open(array(
                    'id'=>'form-content'
                    ,'role'=>'form'
                   ,'enctype'=>'multipart/form-data'
                    ,'url'=>'backend/user/editemploy'
                    ), $rules)}}
                  <div class="box-body">
 
                      <div class="row">
                         
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_firstname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_firstname', $c->e_name, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_lastname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_lastname', $c->e_lname, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                    <div class="row">
                     <div class="form-group col-md-6">
                      <label for="">บริษัท/ห้างร้าน</label>
                      {{Form::input('text', 'txt_email', $c->e_company, 
                         array(
                               'class'=>'form-control',
                             
                               

                              ))}}
                    </div>
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.web_tel', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_tel', $c->e_tel, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                     <div class="row">
                       <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.web_address', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_address', $c->e_address, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                     
                    
                    </div>

                     
                       
                      </div>
                      
                    </div>
                    
                    
                

                 </div><!-- /.box-body -->

                  <div class="box-footer" >
				  <div class="form-group col-md-3">
                     <button type="button" id="bt-reset" class="btn btn-danger  pull-right">
              <i class="fa fa-close"></i> {{ Lang::get('msg.msg_cancle',array(), 'th')}}</button>
              <button type="submit" class="btn btn-primary ">
              <i class="fa fa-check-circle"></i> {{ Lang::get('msg.msg_submit',array(), 'th')}}
               </button>
			   </div>
              </div><!-- /.box -->
              <input type="hidden" name="key" value="{{ Str::random(16,'numberic') }}" >
              <input type="hidden" name="id" value="{{ $id }}" >
              <input type="hidden" name="pid" value="{{ $c->pid }}" >
          
            </div><!-- /.box-body -->
             
                
              {{ Form::close()}}
                  </div>
                
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
 
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_content',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')

         <script type="text/javascript">
             $(document).ready(function(){
          $("#form-content").validetta({ 
            display : 'inline',
           errorTemplateClass : 'validetta-inline'});
         });              
       
         var msg = $('#lang').val();
         var status =$('#status').val();
          
            
              $('#status_save').show(0).delay(2000).slideUp();
               
               
              $('#bt-reset').click(function(){
               
                window.location.href='{{ URL::to("backend/user/employ")}}';
              });



        </script>
 
         
@stop
