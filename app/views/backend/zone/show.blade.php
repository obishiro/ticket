@extends('masterbackend')
@section('content')
       <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
       
        <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title" style="text-transform: uppercase;">บู๊ทที่จองโซน {{ $zone}} ล๊อกที่ {{ $lock}}</h3>

              
            </div>
            <br>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                                <div class=" box-primary">
                 <br>
              
                  {{ Form::open(array(
                    'id'=>'form-content'
                    ,'role'=>'form'
                   ,'enctype'=>'multipart/form-data'
                    ,'url'=>'backend/check/editboot'
                    ), $rules)}}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="row">
                         <label for="">{{ Lang::get('msg.msg_pid', array(), 'th') }}</label>
                    <div class="form-group  ">

                       {{Form::input('text', 'txt_pid', $sql->pid, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required',
                               'maxlength'=>13,
                               'id'=>'txt_pid'

                              ))}}
               
                <!-- /btn-group -->
                
              </div>
                      
                   </div>
                      <div class="row">
                         
                     <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.msg_firstname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_firstname', $sql->e_name, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.msg_lastname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_lastname', $sql->e_lname, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                    <div class="row">
                     <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.web_company', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_company', $sql->e_company, 
                         array(
                               'class'=>'form-control',
                             
                               

                              ))}}
                    </div>
                     <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.web_tel', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_tel', $sql->e_tel, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                    
                    <div class="row">
                         
                         <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.web_address', array(), 'th') }}</label>
                        <textarea class="form-control" name="txt_address" id="" style="width: 100%" rows="3">{{ $sql->e_address}}</textarea>
                        </div>
                       
                         
                     
                        
                        </div>
                         <div class="form-group col-md-12  has-feedback @if ($errors->has('txt_tel')) has-error @endif">
              
                      <label for="">มัดจำการจอง</label>
                      {{Form::input('text', 'txt_mudjam', $sql->lock_mudjam, 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                       
                    {{--  <div class="row">
                         
                         <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.content_picture', array(), 'th') }}</label>
                         <input type="file" name="picture">
                        </div>
                        </div> --}}
                       
                      </div>
                      
                    </div>
                    
                     
                <div class="form-group col-md-8">

                     <a href="{{ URL::to('backend/delzone',array($zone,$lock,$sql->pid))}}" class="btn btn-danger  pull-right" onclick="javascript:return confirm('ต้องการยกเลิกการจองบู๊ท โซน {{$zone}} ล๊อก {{ $lock}}\n ของคุณ{{$sql->e_name}} {{$sql->e_lname}} จริงหรือไม่?')">
              <i class="fa fa-close"></i> {{ Lang::get('msg.msg_cancle',array(), 'th')}}การจอง</a>
 <button type="submit" class="btn btn-primary ">
              <i class="fa fa-check-circle"></i> {{ Lang::get('msg.msg_submit',array(), 'th')}}
               </button>
              
         </div> 

                 </div><!-- /.box-body   marumochi
513411  
          นายชาตรี  จันทรังษี   ฟรีแล้น   0966717819-->

               
          
              <input type="hidden" name="key" value="{{ Str::random(16,'numberic') }}" >
              <input type="hidden" name="txt_zone" value="{{ $zone}}">
              <input type="hidden" name="txt_lock" value="{{ $lock}}">
          
            </div><!-- /.box-body -->
             
                
              {{ Form::close()}}

                  
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                                <div class="col-md-4">
              <!-- Info Boxes Style 2 -->
              <h3>รายละเอียดบู๊ท {{$zone}}</h3>
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">ราคา/บู๊ท</span>
              <span class="info-box-number">{{number_format($price)}} บาท</span>

              <div class="progress">
                
              </div>
              <span class="progress-description">
                   {{--  50% Increase in 30 Days --}}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-bank"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">จำนวนบู๊ท ทั้งหมด</span>
              <span class="info-box-number">{{$count}} ล๊อค</span>

              <div class="progress">
               
              </div>
              <span class="progress-description">
              {{--       20% Increase in 30 Days --}}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-cart-arrow-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">จำนวนบู๊ทที่จองแล้ว</span>
              <span class="info-box-number">{{ Helpers::getCheckZone($zone)}} ล๊อค</span>

              <div class="progress">
            
              </div>
              <span class="progress-description">
                   
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa  fa-credit-card"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">คงเหลือ</span>
              <span class="info-box-number">{{ Helpers::getTotalZone($zone)}} ล๊อค</span>

              <div class="progress">
               
              </div>
              <span class="progress-description">
                   
                  </span>
            </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


        </section><!-- /.content -->
    </div>
 
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_content',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')

         <script type="text/javascript">
             $(document).ready(function(){
           
             
          });              
       
         var msg = $('#lang').val();
         
              $('#status_save').show(0).delay(2000).slideUp();
               
               
              $('#bt-reset').click(function(){
               
                window.location.href='{{ URL::to("backend/boot")}}';
              });



        </script>
 
      
         
@stop
