<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
body {
	font-size:16px;
}
p {font-size:16px;}
table#tb tr.tb_header {
border:1px solid #000000;
background:#d6d6d6;
color:#000000;
font-weight:bold;
font-size:16px;
}
table#tb tr td { border:1px solid #000000; }
.td_totlal {  }
th {font-size:16px;}
.box {
width:20px;
height:20px;
border:1px solid #000000
}
.pay {
	font-size:16px;
	font-weight:bold;
	text-decoration:underline;
}
</style>


<table>
  	<tr>
  		<td colspan="10" align="center">รายงานจองบู๊ทงานกาชาดจังหวัดสุรินทร์ ประจำปี 2561</td>
  	</tr>
  	<tr>
  		<td colspan="10"  align="right">วันที่รายงาน {{ Helpers::changeDate($date) }}</td>
  	</tr>
  	 
  </table>
<table id="tb" width="100%">

	<tr class="tb_header">
		<td width="5%" align="center">#</td>
		<td width="7%" align="center">โซน</td>
		<td width="7%" align="center">ล๊อค</td>
		<td width="15%" align="center">มัดจำ/ราคา</td>
		<th  width="10%" align="center">เหลือ</th>
		<td>ผู้จอง</td>
		<td width="13%" align="center">วันที่จอง</td>
		<td>จองโดย</td>
		<td>ชำระเงิน</td>
		<td  width="20%" align="center">จนท.รับจอง</td>
	</tr>
<?php $i=1;?>
@foreach($data as $d => $c)
	<tr>
		<td  align="center">{{ $i }}</td>
		<td  align="center">{{ $c->zone }}</td>
		<td  align="center">{{ $c->lock }}</td>
		<td  align="center">{{ number_format($c->lock_mudjam) }}/{{ number_format($c->cat_price) }}</td>
		<td  align="center">{{number_format($c->cat_price-$c->lock_mudjam)}}</td>
		<td>{{$c->e_name}} {{$c->e_lname}}</td>
		<td>{{ Helpers::changeDate($c->created_at)}} </td>
		<td width="10%" align="center">{{ Helpers::LockType($c->locktype)}}</td>
		<td width="10%" align="center">@if($c->lockstatus==0) <font style="color:#FF0000"> @else <font style="color:#000000">@endif{{ Helpers::LockStatus($c->lockstatus)}}</font></td>
		<td align="center">{{ $c->uname}} {{ $c->lname}}</td>
	</tr>
	


<?php $i++;?>
@endforeach

<tbody>
</table>
<table>
	<tr class="row2">
		  
		<td colspan="10" align="center" class="td_totlal" style="font-weight: bold">
		 
		รวมสุทธิ {{ number_format($count_price)}} ชำระแล้ว {{ number_format($count_mudjam)}} บาท คงเหลือ {{ number_format($count_price-$count_mudjam)}} บาท</td>
		 

	</tr>
</table>
