

  <header class="main-header">

    <nav class="navbar navbar-static-top">

      <div class="container">

        <div class="navbar-header">

          <a href="{{ URL::to('backend/boot')}}" class="navbar-brand"><b>{{ Lang::get('msg.title',array(),'th')}}</b></a>

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">

            <i class="fa fa-bars"></i>

          </button>

        </div>



        <!-- Collect the nav links, forms, and other content for toggling -->

        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">

          <ul class="nav navbar-nav">

           <li ><a href="{{ URL::to('backend/boot')}}"><i class="fa fa-television"></i> จองบู๊ท <span class="sr-only"></span></a></li>

          {{--   <li class="dropdown">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-television"></i> จองบู๊ท <span class="caret"></span></a>

              <ul class="dropdown-menu" role="menu">

                <li><a href="#">โซน A1</a></li>

                <li><a href="#">โซน A2</a></li>

                <li><a href="#">โซน A3</a></li>

                <li><a href="#">โซน A4</a></li>

                

                <li><a href="#">โซน A5</a></li>

              </ul>

            </li> --}}

            <li ><a href="{{ URL::to('backend/zone')}}"><i class="fa fa-bank"></i> ข้อมูลโซน <span class="sr-only"></span></a></li>



		{{-- 	<li class="dropdown"><a href="{{ URL::to('backend/user')}}"><i class="fa fa-user"></i> ผู้ใช้งาน <span class="sr-only"></span></a></li> --}}



      <li class="dropdown">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ข้อมูลผู้ใช้งาน <span class="caret"></span></a>

              <ul class="dropdown-menu" role="menu">

                <li><a href="{{ URL::to('backend/user')}}"><i class="fa fa-male"></i> ข้อมูลเจ้าที่ป้อนข้อมูล</a></li>

                <li><a href="{{ URL::to('backend/user/employ')}}"><i class="fa fa-group"></i> ข้อมูลรายชื่อผู้จองบู๊ท</a></li>

                 

              </ul>

            </li>





		   {{-- 	<li class="dropdown">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-print"></i> รายงานต่างๆ <span class="caret"></span></a>

              <ul class="dropdown-menu" role="menu">

                <li><a href="{{ URL::to('backend/report')}}">รายงานการจองบู๊ท</a></li>

                 

              </ul>

            </li>    --}}

          </ul>

         {{--  <form class="navbar-form navbar-left" role="search">

            <div class="form-group">

              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">

            </div>

          </form> --}}

        </div>

        <!-- /.navbar-collapse -->

        <!-- Navbar Right Menu -->

        <div class="navbar-custom-menu">

          <ul class="nav navbar-nav">

            <!-- Messages: style can be found in dropdown.less-->

            

            <!-- /.messages-menu -->



            <!-- Notifications Menu -->

            

            <!-- Tasks Menu -->

            

            <!-- User Account Menu -->

            <li class="user user-menu">

              <!-- Menu Toggle Button -->



              <a href="{{ URL::to('backend/logout')}}" >

              ผู้ใช้ 

              {{Helpers::ShowUser()}}

                <span class="hidden-xs"><i class="fa  fa-sign-out"></i> ออกจากระบบ</span>

              </a>

              

            </li>

          </ul>

        </div>

        <!-- /.navbar-custom-menu -->

      </div>

      <!-- /.container-fluid -->

    </nav>

  </header>