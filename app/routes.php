<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/',function(){
  //  return View::make('backend.login');
  return View::make('backend.index');
});
 Route::get('zoneview/{type}/{id}','HomeController@getShowZoneView')->where(array('id'=>'[0-9]+'));
 Route::get('stat','HomeController@getShowStat');
Route::group(array('prefix'=>'backend','before' => 'auth'), function()
{
     
    Route::get('index','BackendController@index');
    Route::get('boot','BackendController@getBoot');
    Route::get('logout','BackendController@getLogout');
    Route::get('checkpid/{pid}','BackendController@getCheckPid');

    Route::get('check/zone/{type}/{id}','BackendController@getCheck')->where(array('id'=>'[0-9]+'));
    Route::post('check/addboot','BackendController@postAddBoot');
    Route::post('check/editboot','BackendController@postEditBoot');
    Route::get('delzone/{zone}/{type}/{pid}','BackendController@getDelBoot')
    ->where(array(
        
        'type' =>'[0-9]+',
        'pid'=>'[0-9]+'));
		    Route::get('delzoneuser/{zone}/{type}/{pid}','BackendController@getDelBootuser')
    ->where(array(
        
        'type' =>'[0-9]+',
        'pid'=>'[0-9]+'));
    Route::get('delzoneemploy/{zone}/{type}/{pid}','BackendController@getDelBootemploy')
    ->where(array(
        
        'type' =>'[0-9]+',
        'pid'=>'[0-9]+'));

       Route::get('show/zone/{type}/{id}','BackendController@getShowZone')->where(array('id'=>'[0-9]+'));
	

	Route::get('frmadd',function(){
	return View::make('backend.frm_add');
	});

	Route::get('user','UserController@index');
    Route::get('user/adduser','UserController@getAdd');
    Route::get('user/edituser/{id}','UserController@getEdit')->where(array('id'=>'[0-9]+'));
    Route::get('user/editemploy/{id}','UserController@getEditemploy')->where(array('id'=>'[0-9]+'));
    Route::post('user/adduser','UserController@postAdd');
    Route::post('user/edituser','UserController@postEdit');
    Route::post('user/editemploy','UserController@postEditemploy');
    Route::get('user/employ','UserController@getEmploy');

	 Route::get('del/{type}/{id}','BackendController@getDel')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::get('show/{type}/{id}','BackendController@getShow')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::get('unshow/{type}/{id}','BackendController@getUnshow')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::post('edit/{type}/','BackendController@postEdit')->where(array('type' =>'[A-Za-z]+'));
    Route::get('edit/{type}/{id}','BackendController@getEdit')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));

    Route::get('zone','ZoneController@index');
    Route::get('zone/addzone','ZoneController@getAdd');
  
    Route::post('zone/addzone','ZoneController@postAdd');

      Route::get('zone/editzone/{id}','ZoneController@getEditZone')->where(array('id'=>'[0-9]+'));
      Route::post('zone/editzone','ZoneController@postEditZone');
      Route::get('showzone/{id}','ZoneController@getShowZone');//->where(array('id'=>'[0-9]+'));

      Route::get('showemploy/{id}','UserController@getShowEmploy')->where(array('id'=>'[0-9]+'));
      Route::post('showemploydata','UserController@postShowEmployData');
     // Route::get('showemploydata','UserController@getShowEmployData');
      Route::get('checkpay/{id}/{pid}','UserController@getCheckPay');
	  Route::get('checkzonepay/{id}/{pid}','UserController@getCheckZonePay');
	
	 Route::get('report','ReportController@index');

      Route::get('export/{type}/{id}','ExportController@getExport')->where(array('type'=>'[A-Za-z]+'));
	  Route::get('exportdate/{type}/{date1}/{date2}/{id}','ExportController@getExportDate');
	  Route::post('showdatazone','ZoneController@postShowdataZone');
});
Route::get('login',function(){
	return View::make('backend.login');
});
Route::post('login','BackendController@postLogin');