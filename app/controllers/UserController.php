<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /user
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = Lang::get('msg.msg_user',array(),'th');
		//$api = URL::to('backend/data/user');
		$sql = User::all();

		return View::make('backend.user.index')->with(
			array(
			'title' 	=>$title,
			'sql'	=> $sql,
			'status'	=> 'null'
			));

		//return '=>'.Auth::user()->user_status;
	}
	public function getAdd()
	{
		 		$title = Lang::get('msg.msg_user',array(),'th');
			//	$sql = Categories::orderBy('id','desc')->get();
			//	$prefix = Prefix::orderBy('prefix_name','asc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.user.adduser')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				//  	'sql'		=> $sql,
				 // 	'prefix'		=> $prefix,
				 	'status'	=> 'null'
				       ));
	}
	public function getEdit($id)
	{
		 		$title = Lang::get('msg.msg_user',array(),'th');
			//	$sql = Categories::orderBy('id','desc')->get();
			//	$prefix = Prefix::orderBy('prefix_name','asc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				$c = User::find($id);
				 
				 
				return View::make('backend.user.edituser')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  //	'sql'		=> $sql,
				  //	'prefix'		=> $prefix,
				 	'status'	=> 'null',
				 	'c'			=>$c,
				 	'id'		=>$id
				       ));
	}
	public function getEditemploy($id)
	{
		 		$title = "ข้อมูลรายชื่อผู้จองบู๊ท";
			//	$sql = Categories::orderBy('id','desc')->get();
			//	$prefix = Prefix::orderBy('prefix_name','asc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				$c = Employee::find($id);
				 
				 
				return View::make('backend.user.editemploy')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  //	'sql'		=> $sql,
				  //	'prefix'		=> $prefix,
				 	'status'	=> 'null',
				 	'c'			=>$c,
				 	'id'		=>$id
				       ));
	}
	public function postAdd()
	{
		 
	 
	 		$token = Input::get('key');
	 	//	$Usertype = Input::get('txt_usertype');
	 		$password = Input::get('txt_password');
			if (Input::hasFile('picture')){
 				  	
		  			$Path='img'; 
		  			//$Path_thumb = $Path.'/thumb';
		  			
		  			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('picture'))->height();
		  			 // $width= Image::make(Input::file('picture'))->width();
		  			 
		  			
		  			Image::make(Input::file('picture'))->resize(100, 100)->save($Path.'/'.$img_name);
		  			 
		  			//Image::make(Input::file('picture'))->resize(50,50)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('picture')->getClientOriginalName();
	  			 }else{
	  			 	$img_name='';
	  			 }
		  	$c = new User;
		  	$c->username 			= Input::get('txt_username');
			$c->uname				= Input::get('txt_firstname');
			$c->lname				= Input::get('txt_lastname');
		  	$c->password 			= Hash::make($password);
	  		//$c->user_type			= Input::get('txt_usertype');
	  		$c->user_status			= Input::get('txt_show');
		   	$c->created_at			= date('Y-m-d H:i:s');
		   	$c->updated_at			= date('Y-m-d H:i:s');
		 //  	$c->created_by			= Auth::user()->id;
	 	   	$c->remember_token 		= Input::get('key');
			$c->email		= Input::input('txt_email');
 			$c->phone		= Input::input('txt_tel');
 			$c->photoURL	= $img_name;
	 		$c->save();
 				  
	  	 
 		 
 		
 		return Redirect::to('backend/user')->with(
				array(
					'save-success' => 'save'
				       ));


	}
	public function postEditemploy($value='')
	{
			$id = Input::get('id');
			$e = Employee::where('id',$id)->update(array(
				'e_name' => Input::get('txt_firstname'),
				'e_lname'=> Input::get('txt_lastname'),
				'e_company'=> Input::get('txt_email'),
				'e_tel'		=> Input::get('txt_tel'),
				'e_address'	=> Input::get('txt_address')

			));
			return Redirect::to('backend/user/employ')->with(
				array(
					'save-success' => 'save'
				       ));

	}

	public function postEdit()
	{
		 
	 
		 	$id = Input::get('id');
	  	 
			$token = Input::get('key');
	 		$Usertype = Input::get('txt_usertype');
	 		$password = Input::get('txt_password');
	 		$newpassword = Hash::make($password);
	 		 if($password ='' || $password ==null){

			$update=	DB::table('users')
			->where('id',$id)
			->update(
                 array(
                 	'username'	=> Input::get('txt_username'),
                 	'uname'	=> Input::get('txt_firstname'),
					'lname'	=> Input::get('txt_lastname'),
					'email'		=> Input::input('txt_email'),
					'phone'		=> Input::input('txt_tel')

                 	));

			}else{
			$update = DB::table('users')
			->where('id',$id)
			->update(
                 array(
                 	'username'	=> Input::get('txt_username'),
                 	'password'	=> $newpassword,
					'uname'	=> Input::get('txt_firstname'),
					'lname'	=> Input::get('txt_lastname'),
					'email'		=> Input::input('txt_email'),
					'phone'		=> Input::input('txt_tel')
                 	 
                 	));
			}
		  	$c = User::find($id);
		   
	  	//	$c->user_type			= Input::get('txt_usertype');
	  		$c->user_status			= Input::get('txt_show');
		    
		 //  	$c->created_by			= Auth::user()->id;
	 	   	$c->remember_token 		= Input::get('key');
	 		$c->save();
 				  if (Input::hasFile('picture')){
		  			$Path='img'; 
		  		 
		  			$old_img = Input::get('img');
		  			if($old_img !='user.png'){
		  				File::delete($Path.'/'.$old_img);
			 		 
		  			}
		  			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('picture'))->height();
		  			 // $width= Image::make(Input::file('picture'))->width();
		  			 
		  			
		  			Image::make(Input::file('picture'))->resize(100, 100)->save($Path.'/'.$img_name);
		  			 
		  	 
	  			 	$filename = Input::file('picture')->getClientOriginalName();
	  			 	DB::table('users')->where('id',$id)->update(array(
	  			 		'photoURL' => $img_name
	  			 		));
	  			 }else{
	  			  
	  			 }
	  	 
 
 
      return Redirect::to('backend/user')->with(
				array(
					'edit-success' => 'edit'
				       ));
   
   	 
   

	}


	public function postAddfile($key)
	{

		  		
		  		if (Input::hasFile('uploadfile')){
		  			$Path='uploadfiles/user'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 
		  			
		  			Image::make(Input::file('uploadfile'))->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/user'; 
					// $upload_success=	Input::file('uploadfile')->move($Path,$img_name);


	     



	    			$Images 	= new Uploadfilesuser;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
 
		  			 
		  		}
 

	}
	public function postEditfile($key)
	{

		  		$oldfile = Uploadfilesuser::where('token',$key)->first();
		  		$Path='uploadfiles/user'; 
		  		$Path_thumb = $Path.'/thumb';
		  		if (Input::hasFile('uploadfile')){

		  	 
		  		
		  			
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 $imagesize=getimagesize(Input::file('uploadfile'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
		  			
		  			Image::make(Input::file('uploadfile'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/user'; 
    			 		$Images = new Uploadfilesuser;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
    			 }

	}

	public function postDropimages($key){

			 $img = Uploadfilesuser::where('files_oldname','=',$key)->first();
		 	 $filename='uploadfiles/user/'.$img->files_newname; 
		 	 $filename_thumb='uploadfiles/user/thumb/'.$img->files_thumb; 
			 File::delete($filename);
			 File::delete($filename_thumb);
			 $img = Uploadfilesuser::where(
			 	array(
			 	//	'token'=>$key,
			 		'files_newname'=>$img->files_newname
			 		))->delete();
			 
		}
		public function postDeleteedituser()
		{
			//$user_file = Uploadfilesuser::where('token',$c->user_file)->get();
		//	 var_dump($key);
			$del_img =Input::get('del_img');
			foreach ($del_img as $del =>$d)
			{
				$filename='uploadfiles/user/'.$d;
		 	 	$filename_thumb='uploadfiles/user/thumb/'.$d; 
			 	File::delete($filename);
			 	File::delete($filename_thumb);
				Uploadfilesuser::where(array('files_newname'=>$d))->delete();

			}
			return 'ok';
		}

		public function getDataedituser($key)
		{
			$user = Uploadfilesuser::where(array('token'=>$key))->get();
			return View::make('backend.user.dataedituser')->with(array(
				'user_file' => $user
				));
		}


		public function getEmploy()
		{
		//	$title = Lang::get('msg.msg_user',array(),'th');
		//$api = URL::to('backend/data/user');
		$sql = Employee::all();

		return View::make('backend.user.employ')->with(
			array(
			'title' 	=>"ข้อมูลรายชื่อผู้จองบู๊ท",
			'sql'	=> $sql,
			'status'	=> 'null'
			));

		}
		public function getShowEmploy($id)
		{
			$sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.lock_mudjam','tb_ticket.created_at','tb_categories.cat_price','users.uname','users.lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->where('tb_ticket.pid',$id)->orderBy('tb_ticket.id','desc')
				->get();

			$count_num = Ticket::where('pid',$id)->count();
			$count_mudjam = Ticket::where('pid',$id)->sum('tb_ticket.lock_mudjam');
			$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
				->where('tb_ticket.pid',$id)->sum('tb_categories.cat_price');
			$c = Employee::where(array('pid'=>$id))->first();
			return View::make('backend.user.showemploy')->with(
				array(
				'title'	=>'การจองบู๊ท',
				'sql'	=>$sql,
				'status'=>'null',
				'c'	=>$c,
				'id'=>$id,
				'count_mudjam' =>str_replace(',','',$count_mudjam),
				'count_num'=>$count_num,
				'count_price'=>$count_price
			)	
			);
		}
		public function getCheckPay($id,$pid)
		{
			Ticket::where('id',$id)->update(array('lockstatus'=>'1'));
			return Redirect::action('UserController@getShowEmploy', array('id' => $pid));

		}
		public function getCheckZonePay($id,$pid)
		{
			Ticket::where('id',$id)->update(array('lockstatus'=>'1'));
			return Redirect::action('ZoneController@getShowZone', array('id' => $pid));

		}
		public function postShowEmployData(){
			$id = Input::get('pid');
	$date1 = Input::get('date1');
	$date2 = Input::get('date2');

	 $sql = DB::table('tb_ticket')
				->select('tb_ticket.id as tid','tb_ticket.lockstatus','tb_ticket.locktype','tb_ticket.zone','tb_ticket.lock','tb_ticket.created_at','tb_ticket.lock_mudjam','tb_categories.cat_price','users.uname','users.lname','tb_employer.pid','tb_employer.e_name','tb_employer.e_lname')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
					->join('users','tb_ticket.create_by','=','users.id')
					->join('tb_employer','tb_ticket.pid','=','tb_employer.pid')
					->where('tb_ticket.pid',$id)
					->whereBetween('tb_ticket.created_at',array($date1,$date2))	
					->orderBy('tb_ticket.id','desc')
				->get();

			$count_num = Ticket::where('pid',$id)->count();
			$count_price  = DB::table('tb_ticket')
					->join('tb_categories','tb_ticket.zone','=','tb_categories.cat_id')
				->where('tb_ticket.pid',$id)->whereBetween('tb_ticket.created_at',array($date1,$date2))->sum('tb_categories.cat_price');
		//	$c = Employee::where(array('pid'=>$id))->first();
		//	return View::make('backend.zone.showzonedata')->with(
				return View::make('backend.user.showemploydata')->with(
				array(
				'title'	=>'การจองบู๊ทโซน',
				'sql'	=>$sql,
				'status'=>'null',
		//		'c'	=>$c,
				'id'=>$id,
				'count_num'=>$count_num,
				'count_price'=>$count_price,
				'date1' =>$date1,'date2'=>$date2
			)	
			);
		}
	}