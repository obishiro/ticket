<?php

return array(
	'title'	=>'ระบบจองบู๊ทงานกาชาด จังหวัดสุรินทร์',
	'txt-please-input' 		=>'กรอกข้อมูล',
	'username'			=> 'ชื่อผู้ใช้งาน',
	'password'			=> 'รหัสผ่าน',
	'signin'				=> 'เข้าสู่ระบบ',
	'content_picture'	=> 'รูปภาพ',
	'msg_firstname'		=> 'ชื่อ',
	'msg_lastname'		=> 'นามสกุล',
	'msg_user'			=> 'ผู้ใช้งาน',

	/////////// process display  //////////
	'error_login_incorrect' 		=> 'ชื่อผู้ใช้งาน และ รหัสผ่าน ผิด',
	'msg_add'			=> 'เพิ่มข้อมูลใหม่',
	'msg_edit'			=> 'แก้ไข',
	'msg_del'			=> 'ลบ',
	'msg_error'			=> 'พบข้อผิดพลาด',
	'msg_cancle'		=> 'ยกเลิก',
	'msg_submit'		=> 'บันทึกการเปลี่ยนแปลง',
	'msg_save_success'		=> 'บันทึกข้อมูลเรียบร้อย',
	'msg_edit_success'		=> 'แก้ไขข้อมูลเรียบร้อย',
	'msg_del_success'		=> 'ลบข้อมูลเรียบร้อย',
	'msg_no'				=> 'ลำดับ',
	'msg_show'			=> 'เผยแพร่',
	'msg_unshow'			=> 'Unshow',
	'msg_created'			=> 'Created at',
	'msg_updated'			=> 'Updated at',
	'msg_tools'				=> 'เครื่องมือ',
	'msg_confirm'			=> 'ต้องการลบข้อมูล จริงหรือไม่?',
	'msg_result'			=> 'Result',
	'msg_logout'			=> 'Log out',
	'msg_type'				=> 'Type',
	'msg_content'			=> 'Content',
	'msg_url'				=> 'Url/Link',
	'msg_submenu'			=> 'Sub menu',
	'msg_sort'				=> 'Sort',
	/////////// Main Menu ////////
	'dashboard'		=> 'Configuration',
	'config-env'		=> 'Config Enviroment',
	'config-categories'	=> 'Config Categories',
	'config-tag'		=> 'Config Tag',
	'config-password'		=> 'Change password',
	'msg_old_password'		=> 'Old password',
	'msg_new_password'		=> 'New password',
	'list-item'		=> 'รายการข้อมูล',
	'mainmenu'		=> 'Main menu',
	'submenu'		=> 'Sub menu',
	'categories_name' => 'Categories name',
	'tag_name' => 'Tag name',
	'mainmenu_name' => 'Mainmenu name',
	'msg_input_categories' => 'Categories name is required',
	'msg_input_tag' => 'Tag name is required',
	'msg_input_mainmenu' => 'This field is required',

	/////////// Content  ///////////
	'content'		=> 'Content',
	'content_name'		=> 'Content name',
	'content_author'		=> 'Author name',
	'content_year'		=> 'Year',
	'content_categories'	=> 'Categories',
	'content_detail'		=> 'Detail',
	'content_file'		=> 'File',
	'content_type'		=> 'Type',
	'content_view'		=> 'View',

	/////////////////////// Enviroment ///////////////////
	'web_name_lo'		=> 'Web name (lao)',
	'web_name_en'		=> 'Web name (Eng)',
	'web_address'		=> 'ที่อยู่',
	'web_tel'			=> 'เบอร์โทรศัพท์',
	'web_email'			=> 'Email',
	'web_keyword'		=> 'Web keyword',
	'web_detail'		=> 'Web detail',
	'web_company'		=> 'บริษัท/ห้างร้าน',
	'msg_pid'			=> 'เลขบัตรประจำตัวประชาชน'



 

	);