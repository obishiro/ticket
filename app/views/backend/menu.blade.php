      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            
            
          </div>
          <!-- search form -->
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">ระบบเมนู</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-gears"></i> <span>{{ Lang::get('msg.dashboard',array(),'th') }}</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 
                <li>
                  <a href="{{ URL::to('backend/config/enviroment') }}">
                    <i class="fa fa-circle-o"></i> {{ Lang::get('msg.config-env',array(),'th') }}
                  </a>
                </li>
                <li>
                  <a href="{{ URL::to('backend/config/categories') }}">
                    <i class="fa fa-circle-o"></i> {{ Lang::get('msg.config-categories',array(),'th') }}
                  </a>
                </li>
                 <li>
                  <a href="{{ URL::to('backend/config/tag') }}">
                    <i class="fa fa-circle-o"></i> {{ Lang::get('msg.config-tag',array(),'th') }}
                  </a>
                </li>
                  <li>
                  <a href="{{ URL::to('backend/config/password') }}">
                    <i class="fa fa-circle-o"></i> {{ Lang::get('msg.config-password',array(),'th') }}
                  </a>
                </li>



              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-navicon"></i> <span>{{ Lang::get('msg.mainmenu',array(),'th') }}</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 
                <li>
                  <a href="{{ URL::to('backend/menu/mainmenu') }}">
                    <i class="fa fa-circle-o"></i> {{ Lang::get('msg.mainmenu',array(),'th') }}
                  </a>
                </li>
                 
                 <li>
                  <a href="{{ URL::to('backend/menu/submenu') }}">
                    <i class="fa fa-circle-o"></i> {{ Lang::get('msg.submenu',array(),'th') }}
                  </a>
                </li>


              </ul>
            </li>
            <li >
              <a href="{{ URL::to('backend/content') }}">
                <i class="fa fa-files-o"></i>
                <span>{{ Lang::get('msg.content',array(), 'th') }}</span>
              
              </a>
               
            </li>
           {{--  <li>
              <a href="pages/widgets.html">
                <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
              </a>
            </li> --}}
            
            <li><a href="{{ URL::to('backend/logout') }}"><i class="fa fa-book"></i> <span>{{ Lang::get('msg.msg_logout',array(),'th') }}</span></a></li>
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>