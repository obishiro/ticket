@extends('masterbackend')
@section('content')
      <div class="content-wrapper" >
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          แผนผังบู๊ทจัดงาน
             
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
        
 
          <div class="row">
            <div class="col-md-12">
              <div class="box" style="width: 100%;padding: 0px">
                <div class="box-header with-border">
                  <h3 class="box-title">แผนผังบู๊ทจัดงาน</h3>
				    <a class="btn bg-black-active btn-flat">A1</a>
                   <a class="btn bg-fuchsia btn-flat">A2</a> 
				   <a class="btn bg-maroon btn-flat">A3</a>
                   <a class="btn bg-purple btn-flat">A4</a>
                   <a class="btn btn-default btn-flat">A5</a>
                   <a class="btn btn-info btn-flat">A6</a>
                   <a class="btn bg-olive btn-flat">A7</a>
                   <a class="btn bg-navy btn-flat">A8</a>

                   <a class="btn btn-success btn-flat">B1</a>
									<a class="btn bg-blue-active  btn-flat">B2</a>
									<a class="btn bg-orange-active  btn-flat">B3</a>
				   <a class="btn btn-primary btn-flat">B4</a>
                    <a class="btn btn-warning btn-flat">B5</a>
                  
           


                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                     <div class="col-md-1"></div>
                     <div class="col-md-1">
                      <img src="{{URL::to('img/maps.png')}}">
                      <br>
                      <div class="box1">บริเวณก่อสร้างใหม่</div>
                    </div>
                     <div class="col-md-1"></div>
                     <div class="col-md-1"></div>
                     <div class="col-md-1"><div class="box2">สระว่ายน้ำ</div></div>
                     <div class="col-md-1"></div>
                     <div class="col-md-1" style="width: 230px"><div class="box3">เวทีมวย</div></div>
                      
                     <div class="col-md-1" style="width: 101px">
                       <div class="box4">สนามเทนนิส</div>
                     </div>
                      <div class="col-md-1"  style="width: 101px">
                       <div class="box5">สนามเทนนิส</div>
                     </div>
                      <div class="col-md-1">
                       <div class="box6">สนามเทนนิส</div>
                     </div>
                     <div class="col-md-1">
                       <div class="box7">สนามยิงปืน</div>
                     </div>
                    

                  </div><!-- /.row -->
                  <br>
                  <div class="row"> 
                    <div class="col-md-1" style="width: 3%"></div>
                    <div class="col-md-7" style="width: 57.3%;"> <!-- แถวแรก -->
                       <div class="row">
                         <div class="col-md-12">
                          <div class="btn-group">
                              <?php for($r1=1;$r1<=61;$r1++): ?>
                              <?php $sql_ZoneA = Ticket::where(array('zone' =>'A5','lock'=>$r1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A5',$r1))}}"  class="btn btn-default btn-flat btn-xs">{{ $r1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A5',$r1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $r1}}</a>
                              <?php } ?>
                            
                           <?php endfor;?>
                          </div>
                         </div>
                         
                       </div>
                    </div><!-- /.col -->
                     <div class="col-md-5"  style="width: 37%;" > <!-- แถวแรก -->
                       <div class="row">
                         <div class="col-md-12">
                          <div class="btn-group">
                              <?php for($rr1=62;$rr1<=97;$rr1++): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'A5','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A5',$rr1))}}"  class="btn btn-default btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A5',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>
                         </div>
                         
                       </div>
                    </div><!-- /.col -->
                   </div>
                   <br>
                  <div class="row"> <!-- แถว 2 -->
                    <div class="col-md-1" style="width: 3%"></div>
                    <div class="col-md-2" style="width: 5%"> <!-- แถวแรก -->
                       <div class="row">
                         <div class="col-md-12">
                          <div class="btn-group">
                              <?php for($r1=1;$r1<=3;$r1++): ?>
                           
                             <?php $sql_ZoneA = Ticket::where(array('zone' =>'A6','lock'=>$r1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A6',$r1))}}"  class="btn btn-info btn-flat btn-xs" >{{ $r1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A6',$r1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $r1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                           

                          </div>
                         </div>
                         
                       </div>
                    </div><!-- /.col -->
                    <div class="col-md-4" style="width: 20%">
                       <div class="btn-group">
                              <?php for($r1=4;$r1<=21;$r1++): ?>
                           
                              <?php $sql_ZoneA = Ticket::where(array('zone' =>'A6','lock'=>$r1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A6',$r1))}}"  class="btn btn-info btn-flat btn-xs" >{{ $r1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A6',$r1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $r1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                           <a class="btn disabled"></a>

                          </div>
                    </div>
                     <div class="col-md-4"  style="width: 33%;" > <!-- แถวแรก -->
                       <div class="row">
                         <div class="col-md-12">
                          <div class="btn-group">
                              <?php for($rr1=22;$rr1<=54;$rr1++): ?>
                           
                              <?php $sql_ZoneA = Ticket::where(array('zone' =>'A6','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A6',$rr1))}}"  class="btn btn-info btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A6',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>
                         </div>
                         
                       </div>
                    </div><!-- /.col -->
                     <div class="col-md-5"  style="width: 37%;" > <!-- แถวแรก -->
                       <div class="row">
                         <div class="col-md-12">
                          <div class="btn-group">
                              <?php for($rr1=55;$rr1<=90;$rr1++): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'A6','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A6',$rr1))}}"  class="btn btn-info btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A6',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>
                         </div>
                         
                       </div>
                    </div><!-- /.col -->
                   </div>
                   <br>

                   <div class="row"> <!-- แถว 3 -->
                     <div class="col-md-1" style="width: 4.3%"></div>
                      <div class="col-md-1" style="width: 3.7%">
                        <div class="btn-group-vertical">
                          <?php for($rr1=1;$rr1<=27;$rr1++): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'B5','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('B5',$rr1))}}"  class="btn btn-warning btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('B5',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>

                        </div>
                      </div>
                       <div class="col-md-1" style="width: 1.5%">
                        <div class="btn-group-vertical">
                          <?php for($rr1=1;$rr1<=24;$rr1++): ?>
                           
                                <?php $sql_ZoneA = Ticket::where(array('zone' =>'B4','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('B4',$rr1))}}"  class="btn btn-primary btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('B4',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>

                        </div>
                      </div>

                      <div class="col-md-2" style="width: 16.5%">
                         <div class="box8"><br><br><br>โรงยิมเผ่าแผน</div>
                      </div>
                      <div class="col-md-1" style="width: 2.1%">
                        <div class="btn-group-vertical">
                          <?php for($rr1=29;$rr1>=3;$rr1--): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'A3','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A3',$rr1))}}"  class="btn bg-maroon btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A3',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>

                        </div>
                      </div>
                       <div class="col-md-1" style="width: 1.5%">
                        <div class="btn-group-vertical">
                          <?php for($rr1=28;$rr1>=3;$rr1--): ?>
                           
                                <?php $sql_ZoneA = Ticket::where(array('zone' =>'A4','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A4',$rr1))}}"  class="btn bg-purple btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A4',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>

                        </div>
                      </div>
                       <div class="col-md-6" style="width: 20.5%">
                        <div class="box9"><br><br><br>โดมศูนย์อาหารอร่อย ราคาถูก</div>
                         <div class="box20">บริเวณสอยดาว</div>
                        <div class="box10">สำนักงาน กกท.</div>
                        <div class="box19">เวทีกลาง</div>
                      </div>
                      <div class="col-md-4">
                        <div id="myDiv">
                          <div class="btn-group">
                          <?php for($rr1=1;$rr1<=29;$rr1++): ?>
                            <?php $sql_ZoneA = Ticket::where(array('zone' =>'A8','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A8',$rr1))}}"  class="btn bg-navy btn-flat btn-xs" style="padding: 2.7px" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A8',$rr1))}}"  class="btn btn-danger btn-flat btn-xs" style="padding: 2.7px">{{ $rr1}}</a>
                              <?php } ?>
                          <?php endfor;?>
                        </div>
                      </div>
                      </div>
                       <div class="col-md-7">
                        <div id="myDiv2">
                          <div class="btn-group">
                          <?php for($rr1=1;$rr1<=32;$rr1++): ?>
                            <?php $sql_ZoneA = Ticket::where(array('zone' =>'A7','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A7',$rr1))}}"  class="btn bg-olive btn-flat btn-xs" style="padding: 2.3px" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A7',$rr1))}}"  class="btn btn-danger btn-flat btn-xs" style="padding: 2.3px">{{ $rr1}}</a>
                              <?php } ?>
                          <?php endfor;?>
                        </div>
                      </div>

                      </div>
                       <div class="col-md-2">
                      <div class="box11"> <br>
                      <br>
                      <br> สนามฟุตบอลศรีณรงค์</div>
                      </div>
                     
                   </div> <!-- แถว 3 -->
                  <br>
                 <div class="row"> 
                    <div class="col-md-1" style="width: 5%"></div>
                    <div class="col-md-7" style="width: 58%;margin-top:-2.3%;margin-left: 2.9%"> <!-- แถวแรก -->
                       <div class="row">
                         <div class="col-md-4">
                          <div class="btn-group">
                              <?php for($rr1=25;$rr1<=42;$rr1++): ?>
                           
                                <?php $sql_ZoneA = Ticket::where(array('zone' =>'B2','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('B2',$rr1))}}"  class="btn bg-blue-active btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('B2',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>
                         </div>
                         
                       </div>
                    </div><!-- /.col -->
                     <div class="col-md-5"  style="width:65%;"> <!-- แถวแรก -->
                       <div class="row" >
                         <div class="col-md-8" style="margin-top:-3.6%;margin-left: 37.4%">
                          <div class="btn-group">
                              <?php for($rr1=1;$rr1<=47;$rr1++): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'A1','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A1',$rr1))}}"  class="btn bg-black-active btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A1',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>
                         </div>
                         
                       </div>
                    </div><!-- /.col -->
                     <div class="col-md-4" style="margin-top:-2.4%;margin-left: 75.2%">
                       <div class="btn-group">
                              <?php for($rr1=33;$rr1<=51;$rr1++): ?>
                           
                                <?php $sql_ZoneA = Ticket::where(array('zone' =>'A7','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A75',$rr1))}}"  class="btn bg-olive btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A7',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>
                    </div>
                   

                    
                   </div>
             
                  <div class="row"  >
                  <div class="col-md-1" style="width: 5.5%"></div>
                    <div class="col-md-2" style="width:20.5%;">
                      <div class="btn-group">
                              <?php for($rr1=28;$rr1<=47;$rr1++): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'B3','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('B3',$rr1))}}"  class="btn bg-orange-active btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('B3',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>

                  </div>

                  <div class="col-md-1" style="width: 2.1%">
                     <div class="btn-group-vertical">
                          <?php for($rr1=2;$rr1>=1;$rr1--): ?>
                           
                              <?php $sql_ZoneA = Ticket::where(array('zone' =>'A3','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A3',$rr1))}}"  class="btn bg-maroon btn-flat btn-xs" >&nbsp;{{ $rr1}}&nbsp;</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A3',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">&nbsp;{{ $rr1}}&nbsp;</a>
                              <?php } ?>
                             
                           <?php endfor;?>

                        </div>

                  </div>
                    <div class="col-md-1" style="width: 1.1%">
                     <div class="btn-group-vertical">
                          <?php for($rr1=2;$rr1>=1;$rr1--): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'A4','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A4',$rr1))}}"  class="btn bg-purple btn-flat btn-xs" >&nbsp;{{ $rr1}}&nbsp;</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A4',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">&nbsp;{{ $rr1}}&nbsp;</a>
                              <?php } ?>
                             
                           <?php endfor;?>

                        </div>

                  </div>
                   <div class="col-md-8"  >
                      <div class="btn-group">
                              <?php for($rr1=1;$rr1<=60;$rr1++): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'A2','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('A2',$rr1))}}"  class="btn bg-fuchsia btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('A2',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>

                  </div>

                   
                  </div><!-- /.row -->
                            <div class="row"  >
              
                    <div class="col-md-2" style="width:28.1%;">
                      <div class="btn-group">
                              <?php for($rr1=1;$rr1<=31;$rr1++): ?>
                           
                               <?php $sql_ZoneA = Ticket::where(array('zone' =>'B1','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('B1',$rr1))}}"  class="btn btn-success btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('B1',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>

                  </div>

              
                   <div class="col-md-8" style="">
                      <div class="btn-group">
                              <?php for($rr1=32;$rr1<=67;$rr1++): ?>
                           
                              <?php $sql_ZoneA = Ticket::where(array('zone' =>'B1','lock'=>$rr1))->count();?>
                              <?php if($sql_ZoneA<=0) { ?>
                              <a href="{{ URL::to('backend/check/zone',array('B1',$rr1))}}"  class="btn btn-success btn-flat btn-xs" >{{ $rr1}}</a>
                              <?php }else{ ?>
                              <a href="{{ URL::to('backend/show/zone',array('B1',$rr1))}}"  class="btn btn-danger btn-flat btn-xs">{{ $rr1}}</a>
                              <?php } ?>
                             
                           <?php endfor;?>
                          </div>

                  </div>

                   
                  </div><!-- /.row -->
                  <br><h3 class="box-title">แผนผังบู๊ทจัดงาน</h3>
                  
                  	    <a class="btn bg-black-active btn-flat">A1</a>
                   <a class="btn bg-fuchsia btn-flat">A2</a> 
				   <a class="btn bg-maroon btn-flat">A3</a>
                   <a class="btn bg-purple btn-flat">A4</a>
                   <a class="btn btn-default btn-flat">A5</a>
                   <a class="btn btn-info btn-flat">A6</a>
                   <a class="btn bg-olive btn-flat">A7</a>
                   <a class="btn bg-navy btn-flat">A8</a>

                   <a class="btn btn-success btn-flat">B1</a>
									<a class="btn bg-blue-active  btn-flat">B2</a>
									<a class="btn bg-orange-active  btn-flat">B3</a>
				   <a class="btn btn-primary btn-flat">B4</a>
                    <a class="btn btn-warning btn-flat">B5</a>
                   <br>
                </div><!-- ./box-body -->
                
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div><!-- /.row -->

 
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  <input type="hidden" name="userid" value="{{ Auth::user()->id}}">
	 
	   
   
</div>
    @stop
 