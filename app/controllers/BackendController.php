<?php



class BackendController extends \BaseController {



	/**

	 * Display a listing of the resource.

	 * GET /backend

	 *

	 * @return Response

	 */

	public function index()

	{

		return View::make('backend.content');

	}



	 

	public function postLogin()

		{   

     		 

    	 	$rules = array(

			'username'	=> 'required',

			'password'	=> 'required'



								);

				$validator = Validator::make(Input::all(), $rules);



    		if ($validator->fails())

    			{

       		 		return Redirect::to('login')->withErrors($validator)->withInput();

     			}else{

 

		       if(Auth::attempt(array('username'=>Input::get('username'),'password'=>Input::get('password')))) {

		          

		       			switch(Auth::user()->user_status):

		       				case '0':

		       					return Redirect::to('login');

		       				break;

		       				case '1':

		       					return Redirect::to('backend/boot');

		       				break;

		       			endswitch;

		       

		            }else{

				        return Redirect::to('login')

				        ->with('error_login_incorrect',Lang::get('msg.error_login_incorrect',array(),'th'));

				      }

				  }

			  } 



 public function getBoot()

 {

 	

 	return View::make('backend.boot')->with(

 		array(

 			 

 		));

 		 

	 

 }

public function getShow($type,$id)

{

	switch ($type) {

 

		case 'user':

			$c= User::find($id);

			$c->user_status = 1;

			$c->save();

			return Redirect::to('backend/user');

			break;

		 

		

	}

		

}

public function getUnshow($type,$id)

{

	switch ($type) {

		 

		case 'user':

			$c= User::find($id);

			$c->user_status = 0;

			$c->save();

			return Redirect::to('backend/user');

			break;

	 



	}

	

	 

}

public function getDel($type,$id)

{

	switch($type):

		 

		case 'user':

		

		$pic = DB::table('users')->where('id',$id)->first();

		$Path='img'; 

		 

		$file_delete_name = $Path."/".$pic->photoURL;

	 

		File::delete($file_delete_name);

	 

		User::find($id)->delete();

		return Redirect::to('backend/user')->with(

				array(

					'del-success' => 'del'

				       ));

		break;
		case 'employ':
			Employee::where('pid',$id)->delete();
			Ticket::where('pid',$id)->delete();
			return Redirect::to('backend/user/employ')->with(

				array(

					'del-success' => 'del'

				       ));
			break;

		 

	endswitch;

}



public function postEditboot()

{

 
		$zone =Input::get('txt_zone');
		$lock = Input::get('txt_lock');
 		$t= Ticket::where('zone',$zone)->where('lock',$lock)
 		 ->update(array('lock_mudjam'=>Input::get('txt_mudjam')));
 		// return Redirect::to('BackendController@getShowZone',array('type'=>$zone,'id'=>$lock));
		 return Redirect::action('BackendController@getShowZone', array('type'=>$zone,'id'=>$lock));
		 

}



public function getCheck($type,$id)

{



	$cat = Categories::where('cat_id',$type)->first();

	 return View::make('backend.zone.add')->with(

	 array(

	 	'title' => 'แบบฟอร์มจองบู๊ท',

	 	'rules'		=>'',

	 	'type'		=>$type,

	 	'lock'		=>$id,

	 	'price'   	=>$cat->cat_price,

	 	'count'		=>$cat->cat_num,

	 ));

}

public function getCheckPid($pid)

{

	 $count = Employee::where('pid',$pid)->count();



	 if($count <= 0)

	 {

	 		return '0';

	 	}else{

	 		return '1';

	 }

}

public function postAddBoot()

{

$z=	Input::get('txt_zone');

$l=Input::get('txt_lock');

	$rules = array(

			'txt_pid'	=> 'required',

			'txt_firstname'	=> 'required',

			'txt_lastname'	=> 'required',

			'txt_company'	=> 'required',

			'txt_tel'	=> 'required',

			'txt_address'	=> 'required',
			'txt_mudjam'	=> 'required',



								);

				$validator = Validator::make(Input::all(), $rules);



    		if ($validator->fails())

    			{

       		 		return Redirect::action('BackendController@getCheck', array($z,$l))->withErrors($validator)->withInput();

     			}else{

	$pid = Input::get('txt_pid');

	$count = Employee::where('pid',$pid)->count();

	if($count <= 0)

	 {

	 	$e = new Employee;

	 	$e->pid = $pid;

	 	$e->e_name = Input::get('txt_firstname');

	 	$e->e_lname = Input::get('txt_lastname');

	 	$e->e_company = Input::get('txt_company');

	 	$e->e_tel = Input::get('txt_tel');

	 	$e->e_address = Input::get('txt_address');



	 	 $e->created_at = date('Y-m-d');

		$e->updated_at = date('Y-m-d H:i:s');

		$e->create_by = Auth::user()->id;

		$e->save();

		$t = new Ticket;
		$t->pid = $pid;
		$t->zone =Input::get('txt_zone');
		$t->lock = Input::get('txt_lock');
		$t->locktype = Input::get('txt_locktype');
		$t->lockstatus = Input::get('txt_lockstatus');
		 $t->created_at = date('Y-m-d');
		$t->updated_at = date('Y-m-d H:i:s');
		$t->create_by = Auth::user()->id;
		$t->lock_mudjam = str_replace(',','',Input::get('txt_mudjam'));
		$t->save();


		 



	 }else{

	 	$t = new Ticket;

		$t->pid = $pid;

		$t->zone =Input::get('txt_zone');

		$t->lock = Input::get('txt_lock');

		$t->locktype = Input::get('txt_locktype');

		$t->lockstatus = Input::get('txt_lockstatus');

		 $t->created_at = date('Y-m-d');

		$t->updated_at = date('Y-m-d H:i:s');

		$t->create_by = Auth::user()->id;
		$t->lock_mudjam =  str_replace(',','',Input::get('txt_mudjam'));

		$t->save();



	 }

				}





	 return Redirect::to('backend/boot')->with(

				array(

					'save-success' => 'save'

				       ));



}

public function getShowZone($zone,$lock)

{

	$sql = Employee::select('tb_employer.pid','tb_employer.e_name','tb_employer.e_lname','tb_employer.e_tel','tb_employer.e_address','tb_employer.e_company','tb_ticket.zone','tb_ticket.lock','tb_ticket.lock_mudjam')
	->where(array('tb_ticket.zone'=>$zone,'tb_ticket.lock'=>$lock))
	->join('tb_ticket','tb_ticket.pid','=','tb_employer.pid')->first();

	$cat = Categories::where('cat_id',$zone)->first();

	return View::make('backend.zone.show')->with(

	 array(

	 	'title' => 'ข้อมูลการจองบู๊ท',

	 	'rules'		=>'',

	 	'zone'		=>$zone,

	 	'lock'		=>$lock,

	 	'sql'		=>$sql,

	 	'price'   	=>$cat->cat_price,

	 	'count'		=>$cat->cat_num,	



	 ));

}



public function getDelBoot($zone,$lock,$pid)

{

		 

	 	Ticket::where(array('pid'=>$pid,'zone'=>$zone,'lock'=>$lock))->delete();

	 	$t = new Unticket;

		$t->pid = $pid;

		$t->zone =$zone;

		$t->lock = $lock;

		 $t->created_at = date('Y-m-d H:i:s');

		$t->updated_at = date('Y-m-d H:i:s');

		$t->create_by = Auth::user()->id;

		$t->save();



	 





	 return Redirect::to('backend/boot')->with(

				array(

					'save-success' => 'save'

				       ));



}

public function getDelBootuser($zone,$lock,$pid)

{

		 

	 	Ticket::where(array('pid'=>$pid,'zone'=>$zone,'lock'=>$lock))->delete();

	 	$t = new Unticket;

		$t->pid = $pid;

		$t->zone =$zone;

		$t->lock = $lock;

		 $t->created_at = date('Y-m-d H:i:s');

		$t->updated_at = date('Y-m-d H:i:s');

		$t->create_by = Auth::user()->id;

		$t->save();



	 

	return Redirect::action('UserController@getShowEmploy', array('id' => $pid));



}

public function getDelBootemploy($zone,$lock,$pid)

{

		 

	 	Ticket::where(array('pid'=>$pid,'zone'=>$zone,'lock'=>$lock))->delete();

	 	$t = new Unticket;

		$t->pid = $pid;

		$t->zone =$zone;

		$t->lock = $lock;

		 $t->created_at = date('Y-m-d H:i:s');

		$t->updated_at = date('Y-m-d H:i:s');

		$t->create_by = Auth::user()->id;

		$t->save();



	 

	return Redirect::action('ZoneController@getShowZone', array('id' => $zone));



}



function getLogout(){

    Auth::logout();

    return Redirect::to('/');

  }







		 

}