@extends('masterfrontend')
@section('content')
       <div class="content-wrapper">
        <!-- Content Header (Page header) -->
         <section class="content-header">
      <h1>สถิติการจองล๊อค</h1>
    </section>

              <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"  style="font-size: 22px">ล๊อคทั้งหมด</span>
              <span class="info-box-number" style="font-size: 22px"  >{{$count_lock}}</span>

              
                  
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
       <div class="row">
        <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"  style="font-size: 22px">จ่าย/จอง</span>
              <span class="info-box-number" style="font-size: 22px">{{$count_lock_pay}}/{{Helpers::getAllTicket()}}</span>

              
                  
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
       <div class="row">
        <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa  fa-cart-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"  style="font-size: 22px">เหลือ</span>
              <span class="info-box-number" style="font-size: 22px">{{$count_lock-Helpers::getAllTicket()}}</span>

              
                   
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
       <div class="row">
        <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box bg-purple">
            <span class="info-box-icon"><i class="fa  fa-frown-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"  style="font-size: 22px">ยังไม่ชำระเงิน</span>
              <span class="info-box-number" style="font-size: 22px">{{$sumall}}</span>

              
                   
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box bg-purple">
            <span class="info-box-icon"><i class="fa  fa-frown-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"  style="font-size: 22px">มัดจำ</span>
              <span class="info-box-number" style="font-size: 22px">{{$count_mudj}}</span>

              
                   
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
       <div class="row">
        <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box bg-maroon">
            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"  style="font-size: 22px">ชำระเงินแล้ว</span>
              <span class="info-box-number" style="font-size: 22px">{{$sumall_total}}</span>

              
                   
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>

        <!-- /.col -->
      </div>
      <!-- /.row -->

        </section><!-- /.content -->
    </div>
@stop