@extends('masterbackend')
@section('content')
       <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
       
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ Lang::get('msg.list-item',array(), 'th') }}</h3>
               
               
              

              @if(Auth::user()->usertype==1)
              <div class="box-tools pull-right">
               <a href="{{ URL::to('backend/user/adduser') }}"   class="btn btn-success"><i class="fa fa-plus-square"></i> {{ Lang::get('msg.msg_add',array(),'th') }}</a>
               @endif
               
           </div>
            </div>
            
            <div class="row" >
              <div class="col-md-5 col-sm-6 col-xs-12" 
              @if(Session::has('status'))
              id ="null"
              @endif
              @if(Session::has('save-success'))
               id="status_save" 
              @endif
              @if(Session::has('edit-success'))
               id="status_save" 
              @endif
              @if(Session::has('del-success'))
               id="status_save" 
              @endif
                style="margin-top:10px;margin-left:30%;  display:none" >
                 @if(Session::has('save-success'))
                  <div class="info-box bg-green">
                 @endif
                 @if(Session::has('edit-success'))
                  <div class="info-box bg-teal">
                 @endif
                 @if(Session::has('del-success'))
                  <div class="info-box bg-red-active">
                 @endif
                <span class="info-box-icon">
                  @if(Session::has('save-success'))
                  <i class="fa fa-save"></i>
                  @endif
                  @if(Session::has('del-success'))
                  <i class="fa fa-trash"></i>
                  @endif
                  @if(Session::has('edit-success'))
                  <i class="fa fa-pencil">
                  @endif
                  </i>
                </span>
                <div class="info-box-content">
                  <span class="info-box-text">{{ Lang::get('msg.msg_result', array(), 'th') }}</span>
                  <span class="info-box-number">
                    @if(Session::has('save-success'))
                    {{ Lang::get('msg.msg_save_success', array(), 'th') }}
                    @endif
                    @if(Session::has('del-success'))
                    {{ Lang::get('msg.msg_del_success', array(), 'th') }}
                    @endif
                    @if(Session::has('edit-success'))
                    {{ Lang::get('msg.msg_edit_success', array(), 'th') }}
                    @endif
                  </span>
                   </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              @if(Session::has('save-success') || Session::has('del-success') || Session::has('edit-success'))
               </div>
              @endif

              
            </div>
            <div class="box-body">

              <table id="Mainmenu_data" class="table table-bordered table-striped" width="100%">
                <thead>
             
                   
                    <th width="5%">{{ Lang::get('msg.msg_no', array(), 'th') }}</th> 
                  <th  >{{ Lang::get('msg.content_picture', array(), 'th') }} </th>
                  <th width="" >{{ Lang::get('msg.msg_firstname', array(), 'th') }} </th>
                   
                  <th>{{ Lang::get('msg.msg_lastname',array(), 'th')}}</th>
           
                  <th>ตำแหน่ง</th>
                  <th>{{ Lang::get('msg.web_tel',array(), 'th')}}</th>
                  <th>{{ Lang::get('msg.msg_show',array(), 'th')}}</th>
                 
                   
                  <th >{{ Lang::get('msg.msg_tools', array(), 'th') }}</th>
                </thead>
				<tbody><?php $i=1;?>
				@foreach($sql as $data=>$d)
				
				<tr>
				<td  align="center">
				{{ $i}}
				</td>
				<td align="center">
				<img src="{{ URL::to('img',array($d->photoURL)) }}" class="img-circle" style="width:32px;height:32px">
				</td>
				<td>{{$d->uname}}</td>
				<td>{{$d->lname}}</td>
				 
				<td>{{ $d->email}}</td>
				<td>{{ $d->phone}}</td>
				<td align="center">
				@if($d->user_status==1)
				<a href="{{ URL::to('backend/unshow/user',array($d->id))}}">
				<i class="fa fa-check-circle fa-2x"></i></a>
				@else  <a href="{{ URL::to('backend/show/user',array($d->id))}}">
				<i class="fa fa-times-circle fa-2x"></i></i></a> @endif</td>
				<td align="center">
				<a href="{{ URL::to('backend/user/edituser',array($d->id)) }}" class="btn btn-default"><i class="fa fa-pencil"></i> </a>
				 <a href="{{ URL::to('backend/del/user',array($d->id)) }}" class="btn btn-danger" onclick="javascript:return confirm('{{ Lang::get('msg.msg_confirm',array(),'th') }}')" ><i class="fa fa-trash"></i></a>
				</td>
				</tr>
				<?php $i++; ?>
				@endforeach
				</tbody>
              </table>
            </div><!-- /.box-body -->
             
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
{{ Session::get('status') }}
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_tag',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')
<script>
  $(function () {
  
    $('#Mainmenu_data').DataTable()
  })
</script>
@stop
 